<?php

include 'mailfunctions.php';

function generateRandomString($length = 10) {
    return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
}

$response = array();

if (isset($_POST['email'])) {
	$email = $_POST['email'];
	
	// include db connect class
    require_once('db.php');
	
	$emailExists = mysqli_query($con, "SELECT * FROM `userinfo` WHERE email = '$email'");
	
	//email id does not exist -> cannot reset password
	if (mysqli_num_rows($emailExists) <= 0) {
		$response["success"] = 0;
		$response["message"] = "email";
		
		echo json_encode($response);
	}  else {
		$temp_password = generateRandomString();
		$query = "UPDATE `userinfo` SET `password`='$temp_password' WHERE `email`='$email'";
		$result = mysqli_query($con, $query);
		
		//SEND EMAIL WITH NEW PASSWORD
		$row = mysqli_fetch_array($emailExists);
		$firstName = $row["firstName"];
		$lastName = $row["lastName"];
		$subject = "DailyDiaryStudy- Password Reset";
		$body = "Hello $firstName $lastName," . PHP_EOL . PHP_EOL . "\tYour new temporary password is:  $temp_password"
			. PHP_EOL . "\t For better security practices, we recommend that you change your password within 72 hours after receiving this email." 
			. PHP_EOL . PHP_EOL . "Regards," . PHP_EOL . "Daily Diary Team";
			
		$sent = sendMail($firstName, $email, $subject, $body);
		
		if($sent){
			$response["success"] = 1;
			$response["message"] = "sent";
		}else{
			$response["success"] = 0;
			$response["message"] = "Trouble with verification process. Please try again later.";
		}
		
		echo json_encode($response);
	}
	
} else {
	$response["success"] = 0;
	$response["message"] = "Required field(s) is missing";
	
	echo json_encode($response);
}

?>