<?php
require_once('mailfunctions.php');
require_once('db.php');

$sql = "SELECT email,firstName,lastName,participantID FROM userinfo";
$result = $con->query($sql);

while($row = $result->fetch_assoc()){
  $pid = intval($row["participantID"]);
  $sql = "SELECT surveyID,lastSubmitDate FROM surveys_reg WHERE participantID=$pid";
  $surveys = $con->query($sql);
  
  while($survey = $surveys->fetch_assoc()){
    $surveydateinfo = date_parse($survey["lastSubmitDate"]);
    $currentdateinfo = getdate();
    
    if(intval($surveydateinfo["month"]) != intval($currentdateinfo["month"]) and intval($surveydateinfo["year"]) == intval($currentdateinfo["year"])){
      sendMail($row["firstName"] . " " . $row["lastName"], $row["email"], "Missed Surveys", "You have a survey which has gone unanswered for at least three days. Please check your survey app and fill it out.");
      break;
    } else if(intval($surveydateinfo["year"]) != intval($currentdateinfo["year"])){
      sendMail($row["firstName"] . " " . $row["lastName"], $row["email"], "Missed Surveys", "You have a survey which has gone unanswered for at least three days. Please check your survey app and fill it out.");
      break;
    } else if(intval($surveydateinfo["day"]) + 3 < intval($currentdateinfo["day"])){
      sendMail($row["firstName"] . " " . $row["lastName"], $row["email"], "Missed Surveys", "You have a survey which has gone unanswered for at least three days. Please check your survey app and fill it out.");
      break;
    }
  }
}
?>