<?php 
	session_start();
	if(isset($_POST)==true && empty($_POST)==false){
		if (isset($_SESSION['first'])==true && $_SESSION['first']==true){				
			$_SESSION['questionText']=array();
			$_SESSION['questionType']=array();
			$_SESSION['randomize']=array();
			$_SESSION['questionGroup']=array();
			$_SESSION['first']= false;
		}		
		
		$qType= $_POST['qType'];		
		switch ($qType){
			case (string) 0:
				array_push($_SESSION['questionType'], $qType);
				array_push($_SESSION['questionText'], $_POST['qText']);
				array_push($_SESSION['questionGroup'], $_POST['qGroup']);
				if (isset($_POST['randomize'])){
					array_push($_SESSION['randomize'], 1);
				}
				else{
					array_push($_SESSION['randomize'], 0);	
				}
				break;

			case (string) 1:
				array_push($_SESSION['questionType'], $qType);
				array_push($_SESSION['questionText'], $_POST['qText']);
				array_push($_SESSION['questionGroup'], $_POST['qGroup']);
				if (isset($_POST['randomize'])){
					array_push($_SESSION['randomize'], 1);
				}
				else{
					array_push($_SESSION['randomize'], 0);	
				}
				break;

			case (string) 2:
				echo "Case 2";
				$output = $_POST['qText'];
				$output .= ";";
				foreach ($_POST['opt'] as $key => $value) {
					$output .= $value.";";
				}				
				echo "\n";
				echo $output;
				echo "\n";
				array_push($_SESSION['questionType'], $qType);
				array_push($_SESSION['questionText'], $output);
				array_push($_SESSION['questionGroup'], $_POST['qGroup']);
				if (isset($_POST['randomize'])){
					array_push($_SESSION['randomize'], 1);
				}
				else{
					array_push($_SESSION['randomize'], 0);	
				}
				break;
			default:
				echo "Unknown type";

		}		
		print_r($_SESSION);
		if (isset($_POST['commit'])){
			header('Location: ./commit.php');
		}
		else{
			header('Location: ./questions.php?visit=Success');	
		}		
		exit();		
	}	
?>