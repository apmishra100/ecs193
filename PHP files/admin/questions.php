<html>
    <head>
        <title>DiaryStudies Control Panel</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <script type="text/javascript" src="js/scripts.js"></script>
    </head>
    <body>    
        <form class="register" action="next.php" id="questionForm" method="POST">
            <h1>DiaryStudies Control Panel- Add Survey</h1>
			<?php if((isset($_POST)==true && empty($_POST)==false) || ($_GET['visit']=="Success")): 
				session_start();							
				if (isset($_POST['studyID'])){
					$studyID = $_POST['studyID'];
					$notifications = $_POST['notifications'];													
					$_SESSION["studyID"] = $studyID;
					$_SESSION["notifications"] = $notifications;					
					$_SESSION["first"] = true;
					$_SESSION["title"] = $_POST['title'];
					$startDate = $_POST['startDate'];
					$startTime = $_POST['startTime'];
					$endDate = $_POST['endDate'];
					$endTime = $_POST['endTime'];
					$phpStartDate = strtotime($startDate);
					$phpStartTime = strtotime($startTime);
					$phpEndDate = strtotime($endDate);
					$phpEndTime = strtotime($endTime);
					$mysqlStartDate = date('Y-m-d', $phpStartDate);
					$mysqlStartTime = date('H:i:s', $phpStartTime);

					$mysqlEndDate = date('Y-m-d', $phpEndDate);
					$mysqlEndTime = date('H:i:s', $phpEndTime);

					$_SESSION["startDate"] = $mysqlStartDate;
					$_SESSION["startTime"] = $mysqlStartTime;
					$_SESSION["endDate"] = $mysqlEndDate;
					$_SESSION["endTime"] = $mysqlEndTime;
				}
				$studyID = $_SESSION["studyID"];
				// $verifCode = $_SESSION["verifCode"];																				
				print_r($_SESSION);				
			?>
			<fieldset class="row1">
                <legend>Survey Information</legend>
				<p>
                    <label>Study ID</label>
                    <input name="id" type="text" readonly="readonly" value="<?php echo $studyID ?>"/>
                </p>
                <p>
                	<label>Notifications</label>
					<input name= "notif" type="text" readonly="readonly" value="<?php if($notifications='1'){echo 'Yes';}else{ echo 'No';}  ?>"/>                             				
                </p>

                <p>
                	<label>Start Date</label>
					<input name= "startDate" type="text" readonly="readonly" value="<?php echo $_SESSION['startDate'] ?>"/>		
                </p>

                <p>
                	<label>End Date</label>
					<input name= "endDate" type="text" readonly="readonly" value="<?php echo $_SESSION['endDate'] ?>"/>		
                </p>

                <p>
                	<label>Available Start Date</label>
					<input name= "startDate" type="text" readonly="readonly" value="<?php echo $_SESSION['startTime'] ?>"/>		
                </p>

                <p>
                	<label>Available End Date</label>
					<input name= "endDate" type="text" readonly="readonly" value="<?php echo $_SESSION['endTime'] ?>"/>		
                </p>


				<div class="clear"></div>
            </fieldset>

            <fieldset>
            	<legend> Question </legend>
            		<table id="dataTable" class="form" border="1">                  		
                    		<tr>
                      			<p>
									<td>
										<label>Question Text</label>
										<input type="text" required="required" name="qText">
						 			</td>
                          			<td>
										<label for="qtype">Question Type</label>
											<select id="qType" name="qType" required="required" onchange="dropcheck(this)" onselect="dropcheck(this)">
												<option value = "0" selected="selected"> Slider </option>
												<option value = "1"> Free Response</option>
												<option value = "2"> Checkbox </option>
											</select>                            		
						 			</td>
						 			<td>
						 				<label for="qGroup">Question Group</label>
						 				<select id="qGroup" name="qGroup" required="required">
						 					<option value = "A" selected="selected">A</option>
						 					<option value = "B">B</option>
						 					<option value = "C">C</option>
						 					<option value = "D">D</option>
						 				</select>
						 			</td>
						 			<td>
						 				<label>Randomize</label>
						 				<input id=randomize name="randomize" type="checkbox" value="1" checked="checked">						 				
						 			</td>
								</p>
                    		</tr>                   		
                	</table>            	
            </fieldset>

            <fieldset class="row5">
                <legend>Submit</legend>
                <p>
					<input class="submit" type="submit" name="add" value="Add Question and Continue &raquo;" />					
                </p>

				<p>
					<input class="submit" type="submit" name="commit" value="Finish Adding Question and Create &raquo;" />					
                </p>

				<div class="clear"></div>
            </fieldset> 
		<?php else: ?>
		<fieldset class="row1">
			<legend>Sorry</legend>
			 <p>Some things went wrong please try again.</p>
		</fieldset>
		<?php endif; ?>
			<div class="clear"></div>
        </form>
    </body>
</html>





