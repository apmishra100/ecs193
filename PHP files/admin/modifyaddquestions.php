<html>
    <head>
        <title>DiaryStudies Control Panel</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <script type="text/javascript" src="js/scripts.js"></script>
    </head>
    <body>    
        <form class="addQuestionMod" action="nextmod.php" id="modaddquestionForm" method="POST">
            <h1>DiaryStudies Control Panel- Modify Survey</h1>
			<?php if(isset($_GET)==true && empty($_GET)==false && $_GET['modify']=="add"): 
				session_start();																			
			?>			
            <fieldset>
            	<legend> Question </legend>
            		<table id="dataTable" class="form" border="1">                  		
                    		<tr>
                      			<p>
									<td>
										<label>Question Text</label>
										<input type="text" required="required" name="qText">
						 			</td>
                          			<td>
										<label for="qtype">Question Type</label>
											<select id="qType" name="qType" required="required" onchange="dropcheck(this)" onselect="dropcheck(this)">
												<option id="opt0" value = "0" selected="selected"> Slider </option>
												<option id="opt1" value = "1"> Free Response</option>
												<option id="opt2" value = "2"> Checkbox </option>												
											</select>                           		
						 			</td>
						 			<td>
						 				<label for="qGroup">Question Group</label>
						 				<select id="qGroup" name="qGroup" required="required">
						 					<option value = "A" selected="selected">A</option>
						 					<option value = "B">B</option>
						 					<option value = "C">C</option>
						 					<option value = "D">D</option>
						 				</select>
						 			</td>
						 			<td>
						 				<label>Randomize</label>
						 				<input id=randomize name="randomize" type="checkbox" value="1" checked="checked">						 				
						 			</td>
								</p>
                    		</tr>                   		
                	</table>            	
            </fieldset>

            <fieldset class="row5">
                <legend>Submit</legend>                
				<p>
					<button class="submit" type="submit" name="add" value="add">Add Question</button>					
                </p>

				<div class="clear"></div>
            </fieldset> 
		<?php else: ?>
		<fieldset class="row1">
			<legend>Sorry</legend>
			 <p>Some things went wrong please try again.</p>
		</fieldset>
		<?php endif; ?>
			<div class="clear"></div>
        </form>
    </body>
</html>





