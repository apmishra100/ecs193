<?php
 
/*
 * Following code will get single product details
 * A product is identified by product id (pid)
 */
 
// array for JSON response
$response = array();
 
// include db connect class
require_once __DIR__ . '/db.php';
  
// check for post data
if (isset($_POST["email"]) && isset($_POST["password"])) {
    $email = $_POST['email'];
	$password = $_POST['password'];
 
    // get a product from products table
    $result = mysqli_query($con, "SELECT *FROM userinfo WHERE email = '$email' and password = '$password'");
	$resultEmail = mysqli_query($con, "SELECT *FROM userinfo WHERE email = '$email'");
 
	if (mysqli_num_rows($result) > 0) {

		$result = mysqli_fetch_array($result);

		$verified = $result["verified"];
		
		if($verified == 0){
			$response["success"] = 1;
			$response["message"] = "verify";
		}else{
			$response["success"] = 1;
			$response["message"] = "loggedIN";
		}

		// echoing JSON response
		echo json_encode($response);
	} else if(mysqli_num_rows($resultEmail) > 0) {
		// no product found
		$response["success"] = 0;
		$response["message"] = "password";

		// echo no users JSON
		echo json_encode($response);
	}else {
        // no product found
        $response["success"] = 0;
        $response["message"] = "No user found";
 
        // echo no users JSON
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}
?>