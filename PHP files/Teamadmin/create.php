<?php
    session_start();
    if(!isset($_SESSION['logged_in'])){

        header("Location: login.php");
        echo 'Please Log in.';

    }//check if user is logged in

?>
<html>
    <head>
        <title>DiaryStudies Control Panel</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <script type="text/javascript" src="js/scripts.js"></script>
        <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <!-- cdn for modernizr, if you haven't included it already -->
        <script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
        <!-- polyfiller file to detect and load polyfills -->
        <script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
        <script>
          webshims.setOptions('waitReady', false);
          webshims.setOptions('forms-ext', {types: 'date'});
          webshims.polyfill('forms forms-ext');
        </script>
        
        <script>
            function init(){
                document.getElementById('checkID').onclick=function(){checkID(document.getElementById('studyID').value);};
            }
            window.onload=init;
        </script>        
    </head>
    <body>    
        <form action="questions.php" id="createForm" method="POST">
            

            <h1>DiaryStudies Control Panel- Create Survey</h1>
			<fieldset class="row1">
                <legend>Survey General Information</legend>
				<p>
                    <label>Study ID (Numerical only)*:</label>
                    <input id="studyID" name="studyID" type="text" required="required" onkeypress='validate(event)'/>
                    <input name="checkID" id="checkID" type="button" value="Check ID"/>
                </p>
                <p>
                    <label>Notifications*:</label>
                    <select name="notifications" required="required"> 
                        <option value="1" selected="selected">Yes</option>
                        <option value="0">No</option>
                    </select>
                </p>
                <p>
                    <label>Title*:</label>
                    <input name="title" id="checkID" type="text" value="" required="required"> </input>                 
                </p>

                <p>
                    <label>Start Date*:</label>
                    <input name="startDate" id="startDate" type="date" required="required"> </input>                                        
                </p>


                <p>
                    <label>End Date </label>
                    <input name="endDate" id="endDate" type="date" required="required"> </input>
                    
                </p>

                <p>
                    <label>Start Availability Time</label>
                    <input name="startTime" id="startTime" type="time" required="required"> </input>
                </p>

                <p>
                    <label>End Availability Time</label>
                    <input name="endTime" id="endTime" type="time" required="required"> </input>
                </p>
                <!-- <p>
                    <label> Number of Questions</label>
                    <input name="numQuestions" type="text" required="required" onkeypress='validate(event)'> </input>
                </p> -->
				<div class="clear"></div>
            </fieldset>
			<input class="submit" type="submit" value="Confirm &raquo;" />						
			<div class="clear"></div>
        </form>
		
    </body>	
</html>





