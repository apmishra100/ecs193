<?php 
	session_start();	 	    
	if(!isset($_SESSION['logged_in']))
	{
		header("Location: ./login.php");
	    echo 'Please Log in.';

	}//check if user is logged in	

	if(isset($_POST)==true && empty($_POST)==false){
		if (isset($_POST['addmod'])){
			$_SESSION['valid'] = true;
			$_SESSION['modified'] = true;
			$index = $_POST['addmod'];
			$qType= $_POST['qType'];		
			switch ($qType){
				case (string) 0:
					$_SESSION['questionType'][$index] = $qType;
					$_SESSION['questionText'][$index] = $_POST['qText'];
					$_SESSION['questionGroup'][$index] = $_POST['qGroup'];
					if (isset($_POST['randomize'])){
						$_SESSION['randomize'][$index]= 1;
					}
					else{
						$_SESSION['randomize'][$index]= 0;	
					}
					break;

				case (string) 1:
					$_SESSION['questionType'][$index]= $qType;
					$_SESSION['questionText'][$index]= $_POST['qText'];
					$_SESSION['questionGroup'][$index] = $_POST['qGroup'];
					if (isset($_POST['randomize'])){
						$_SESSION['randomize'][$index]= 1;
					}
					else{
						$_SESSION['randomize'][$index]= 0;	
					}

					break;

				case (string) 2:					
					$output = $_POST['qText'];
					$output .= ";";
					foreach ($_POST['opt'] as $key => $value) {
						$output .= $value.";";
					}										
					$_SESSION['questionType'][$index] = $qType;
					$_SESSION['questionText'][$index] = $output;
					$_SESSION['questionGroup'][$index] = $_POST['qGroup'];
					if (isset($_POST['randomize'])){
						$_SESSION['randomize'][$index]= 1;
					}
					else{
						$_SESSION['randomize'][$index]= 0;	
					}

					break;
				default:
					echo "Unknown type";
			}
			header('Location: ./modifyquestions.php');
			exit();
		}
		elseif (isset($_POST['delete'])) {
			$_SESSION['valid'] = true;
			$_SESSION['modified'] = true;
			$index = $_POST['delete'];
			unset($_SESSION['questionType'][$index]);
			unset($_SESSION['questionText'][$index]);
			unset($_SESSION['questionGroup'][$index]);
			unset($_SESSION['randomize'][$index]);
			$_SESSION['questionType']=array_values($_SESSION['questionType']);
			$_SESSION['questionText']=array_values($_SESSION['questionText']);
			$_SESSION['questionGroup']=array_values($_SESSION['questionGroup']);
			$_SESSION['randomize']=array_values($_SESSION['randomize']);
			header('Location: ./modifyquestions.php');
			exit();
		}
		elseif (isset($_POST['add'])){
			$_SESSION['valid'] = true;
			$_SESSION['modified'] = true;
			$qType= $_POST['qType'];		
			switch ($qType){
				case (string) 0:
					array_push($_SESSION['questionType'], $qType);
					array_push($_SESSION['questionText'], $_POST['qText']);
					array_push($_SESSION['questionGroup'], $_POST['qGroup']);
					if (isset($_POST['randomize'])){
						array_push($_SESSION['randomize'], 1);
					}
					else{
						array_push($_SESSION['randomize'], 0);	
					}
					break;

				case (string) 1:
					array_push($_SESSION['questionType'], $qType);
					array_push($_SESSION['questionText'], $_POST['qText']);
					array_push($_SESSION['questionGroup'], $_POST['qGroup']);
					if (isset($_POST['randomize'])){
						array_push($_SESSION['randomize'], 1);
					}
					else{
						array_push($_SESSION['randomize'], 0);	
					}
					break;

				case (string) 2:
					echo "Case 2";
					$output = $_POST['qText'];
					$output .= ";";
					foreach ($_POST['opt'] as $key => $value) {
						$output .= $value.";";
					}
					
					echo "\n";
					echo $output;
					echo "\n";
					array_push($_SESSION['questionType'], $qType);
					array_push($_SESSION['questionText'], $output);
					array_push($_SESSION['questionGroup'], $_POST['qGroup']);
					if (isset($_POST['randomize'])){
						array_push($_SESSION['randomize'], 1);
					}
					else{
						array_push($_SESSION['randomize'], 0);	
					}
					break;
				default:
					echo "Unknown type";
			}
			header('Location: ./modifyquestions.php');
			exit();
		}

		else{

		}		
		print_r($_SESSION);
		// if (isset($_POST['commit'])){
		// 	header('Location: ./commit.php');
		// }
		// else{
		// 	header('Location: ./questions.php?visit=Success');	
		// }		
		// exit();		
	}
	else{
		print("There was an error navigating to this page. Please return home.");
	}	
?>