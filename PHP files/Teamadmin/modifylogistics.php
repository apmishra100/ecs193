 <?php
    session_start();
    if(!isset($_SESSION['logged_in']))
    {

        header("Location: ./login.php");
        echo 'Please Log in.';

    }//check if user is logged in
?>
<html>
    <head>
        <title>DiaryStudies Control Panel</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <script type="text/javascript" src="js/scripts.js"></script>
        <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <!-- cdn for modernizr, if you haven't included it already -->
        <script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
        <!-- polyfiller file to detect and load polyfills -->
        <script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
        <script>
          webshims.setOptions('waitReady', false);
          webshims.setOptions('forms-ext', {types: 'date'});
          webshims.polyfill('forms forms-ext');
        </script>
        <script type="text/javascript">
        	
        </script>
        
    </head>
    <body>    	   
        <form class="register" action="modifyquestions.php" id="logisticsForm" method="POST">
            <h1>DiaryStudies Control Panel- Modify Survey</h1>
			<?php if((isset($_POST)==true && empty($_POST)==false) || ($_SESSION['valid']==true)){ 				
				$error = false; 							
				if (isset($_POST['studyID'])){
					$studyID = $_POST['studyID'];					
					$_SESSION['studyID'] = $_POST['studyID'];
					$_SESSION['questionText']=[];
					$_SESSION['questionType']=[];
					$_SESSION['randomize']=array();
					$_SESSION['questionGroup']=array();
					require_once('db.php');
					$sql = "SELECT * from studyid WHERE studyIDUnique = '$studyID'";
					$result = $con->query($sql);
					if (($result->num_rows) > 0){
						$row = $result->fetch_assoc();
						$_SESSION['studyID'] = $row['studyIDUnique'];
						$_SESSION['notifications'] = $row['notifications'];
						$_SESSION['title'] = $row['title'];						
						$phpStartDate = strtotime($row['startDate']);
						$phpStartTime = strtotime($row['availStartTime']);

						$_SESSION['startDate'] = $phpStartDate;
						$_SESSION['startTime'] = $phpStartTime;

						$phpEndDate = strtotime($row['endDate']);
						$phpEndTime = strtotime($row['availEndTime']);

						$_SESSION['endDate'] = $phpEndDate;
						$_SESSION['endTime'] = $phpEndTime;

						$generalSurveyID = $_SESSION['studyID'].'_';
						$sql = "SELECT * from surveys WHERE SurveyID LIKE '$generalSurveyID' Order BY QuestionID ASC";
						$result = $con->query($sql);
						if($result->num_rows > 0){					     						
							while($row = $result->fetch_assoc()) {
								array_push($_SESSION['questionType'], $row['QuestionType']);
								array_push($_SESSION['questionText'], $row['Question']);
								array_push($_SESSION['randomize'], $row['Randomize']);
								array_push($_SESSION['questionGroup'], substr($row['SurveyID'],-1));										
							}   
						}
						print_r($_SESSION);
						$_SESSION['valid']=true;
						header("Location: ./modifylogistics.php");			
					}
					else{
						$error=true;
						print "SurveyID was not found '$studyID'";					
					}
					$con->close();										
				}
				}
				else{
					print "There was an error navigating to this page";
					$error=true;
				}				
			?>
			<?php if ($error == false) : ?>			
			<fieldset class="row1">
                <legend>Survey Information</legend>                
				<p>
                    <label>Study ID (Numerical only)*:</label>
                    <input id="studyID" name="studyID" type="text" required="required" readonly="true" value="<?php echo $_SESSION['studyID'] ?>" > </input>                    
                </p>
                <p>
                	<label>Notifications*:</label>
                    <select name="notifications" required="required"> 
                        <option value="1" <?php if ($_SESSION['notifications']==1){echo 'selected=selected';}?>>Yes</option>
                        <option value="0" <?php if ($_SESSION['notifications']==0){echo 'selected=selected';}?>>No</option>
                    </select>
                </p>
                <p>
                    <label>Title*:</label>
                    <input name="title" id="title" type="text" required="required"  value="<?php echo $_SESSION['title'] ?>"> </input>                 
                </p>

                <p>
                    <label>Start Date:</label>
                    <input name="startDate" id="startDate" type="date" required="required"  value="<?php echo date('Y-m-d', $_SESSION['startDate']); ?>" > </input>                    
                </p>

                <p>
                    <label>End Date</label>
                    <input name="endDate" id="endDate" type="date" required="required"  value="<?php echo date('Y-m-d', $_SESSION['endDate']); ?>"> </input>                    
                </p>

                <p>
                	<label>Available Start Time</label>
                	<input name="startTime" id="startTime" type="time" required="required"  value="<?php echo date('H:i',$_SESSION['startTime']); ?>"> </input>
                </p>

                <p>
                	<label>Available End Time</label>
                	<input name="endTime" id="endTime" type="time" required="required"  value="<?php echo date('H:i', $_SESSION['endTime']); ?>"> </input>
                </p>
				                
				<div class="clear"></div>
            </fieldset>

            <fieldset class="row5">
                <legend>Submit Changes</legend>
                <p>
					<input class="submit" type="submit" name="modifyLogistics" value="Save and Continue &raquo;" />					
                </p>
				
				<div class="clear"></div>
            </fieldset>

		<?php else: ?>
		<fieldset class="row1">
			<legend>Sorry</legend>
			 <p>Some things went wrong please try again.</p>
		</fieldset>
		<?php endif; ?>
			<div class="clear"></div>
        </form>
    </body>
</html>





