<?php
	session_start();
	if(!isset($_SESSION['logged_in']))
	{

		header("Location: ./login.php");
		echo 'Please Log in.';

	}//check if user is logged in

	require('db.php');

	$sql = "SELECT * FROM studyid where 1 ORDER BY startDate ASC";

	$result = $con->query($sql);

	if($result->num_rows > 0){
							     						
		while($row = $result->fetch_assoc()) {
			echo "<fieldset>";
			echo "<table>";
			$studyid = $row['studyIDUnique'];
			echo "<tr> <td> Study ID: ".$row['studyIDUnique']." </td> </tr>";
			echo "<tr> <td> Title: ".$row['title']." </tr>";
			echo "<tr> <td> Notifications: ".$row['notifications']." </td> </tr>";
			echo "<tr> <td> Start Date: ".$row['startDate']." </td> </tr>";
			echo "<tr> <td> End Date: ".$row['endDate']." </td> </tr>";
			echo "<tr> <td> Start Time ".$row['availStartTime']." </td> </tr>";
			echo "<tr> <td> End Time: ".$row['availEndTime']." </td> </tr>";
			echo "</table>";			
			echo "<br>";
			echo "<br>";
			$generalSurveyID = $studyid.'_';
			$questionssql = "SELECT * from surveys WHERE SurveyID LIKE '$generalSurveyID' OR SurveyID='$studyid' ORDER BY QuestionID ASC ";		
			$questionsresult = $con->query($questionssql);
			echo "<br>";
			
			if ($questionsresult->num_rows > 0){
				echo "<table id='questionTable' border='1'>";
				echo "<tr> <th>QuestionID</th> <th> Question Type </th> <th> Question Text </th> <th> Question Group </th> <th> Randomize </th> </tr>";
				while ($questionsrow = $questionsresult->fetch_assoc()){
					echo "<tr> <td>".$questionsrow['QuestionID']."</td> <td>".$questionsrow['QuestionType']."</td> <td>".$questionsrow['Question']." </td> <td>".substr($questionsrow['SurveyID'], -1)."</td> <td>" .$questionsrow['Randomize']." </td> </tr>";            	            	
				}           
				echo "</table>";            			
			}
            echo"<br>";
            echo mysqli_error($con);
            echo "</fieldset>";            
		}   
	}


?>