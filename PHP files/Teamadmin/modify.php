 <?php
    session_start();
    if(!isset($_SESSION['logged_in']))
    {

        header("Location: ./login.php");
        echo 'Please Log in.';

    }//check if user is logged in
?>

<html>
    <head>
        <title>DiaryStudies Control Panel</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <script type="text/javascript" src="js/scripts.js"></script>
        <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <!-- cdn for modernizr, if you haven't included it already -->
        <script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
        <!-- polyfiller file to detect and load polyfills -->
        <script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
        <script>
          webshims.setOptions('waitReady', false);
          webshims.setOptions('forms-ext', {types: 'date'});
          webshims.polyfill('forms forms-ext');
        </script>        
    </head>
    <body>    
        <form action="modifylogistics.php" id="modifyForm" method="POST">
        

            <h1>DiaryStudies Control Panel- Modify Survey</h1>
			<fieldset class="row1">
                <legend>Survey General Information</legend>
				<p>
                    <label>Enter Study ID:</label>
                    <input id="studyID" name="studyID" type="text" required="required" onkeypress='validate(event)'/>                    
                </p>                                                                                
				<div class="clear"></div>
            </fieldset>
			<input class="submit" type="submit" value="Begin Modification &raquo;" />						
			<div class="clear"></div>
        </form>
		
    </body>	
</html>





