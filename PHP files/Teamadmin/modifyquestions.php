 <?php
    session_start();
    if(!isset($_SESSION['logged_in']))
    {

        header("Location: ./login.php");
        echo 'Please Log in.';

    }//check if user is logged in
?>
<html>
    <head>
        <title>DiaryStudies Control Panel</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <script type="text/javascript" src="js/scripts.js"></script>
        <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <!-- cdn for modernizr, if you haven't included it already -->
        <script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
        <!-- polyfiller file to detect and load polyfills -->
        <script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
        <script>
          webshims.setOptions('waitReady', false);
          webshims.setOptions('forms-ext', {types: 'date'});
          webshims.polyfill('forms forms-ext');
        </script>

        
    </head>
    <body>    
        <form class="register" action="modifyquestionhelper.php" id="modQuestionsForm" method="POST">
            <h1>DiaryStudies Control Panel- Modify Survey Questions</h1>
			<?php if((isset($_POST)==true && empty($_POST)==false)||($_SESSION['valid']==true)) { 				
				$error = false;                			
				if (isset($_POST['modifyLogistics'])){
                    $_SESSION['modified'] = false;
                    $_SESSION['valid'] = true;					
					$_SESSION['studyID'] = $_POST['studyID'];
					$_SESSION['notifications'] = $_POST['notifications'];
					$_SESSION["title"] = $_POST['title'];
					$startDate = $_POST['startDate'];
					$startTime = $_POST['startTime'];
					$endDate = $_POST['endDate'];
					$endTime = $_POST['endTime'];
					$phpStartDate = strtotime($startDate);
					$phpStartTime = strtotime($startTime);
					$phpEndDate = strtotime($endDate);
					$phpEndTime = strtotime($endTime);
					$mysqlStartDate = date('Y-m-d', $phpStartDate);
					$mysqlStartTime = date('H:i:s', $phpStartTime);

					$mysqlEndDate = date('Y-m-d', $phpEndDate);
					$mysqlEndTime = date('H:i:s', $phpEndTime);

					$_SESSION["startDate"] = $mysqlStartDate;
					$_SESSION["startTime"] = $mysqlStartTime;
					$_SESSION["endDate"] = $mysqlEndDate;
					$_SESSION["endTime"] = $mysqlEndTime;
                    header("Location: ./modifyquestions.php");
					
				}
				print_r($_SESSION);

				}				
				else{
					print "There was an error navigating to this page";
					$error=true;
				}				
			?>
			<?php if ($error == false) : ?>			
			<fieldset class="row1">
                <legend>Survey Information</legend>                
				<p>
                    <label>Study ID (Numerical only)*:</label>
                    <input id="studyID" name="studyID" type="text" required="required" readonly="true" value="<?php echo $_SESSION['studyID'] ?>" > </input>                    
                </p>
                <p>
                    <label>Notifications*:</label>
                    <select name="notifications" readonly="true" required="required"> 
                        <option value="1" <?php if ($_SESSION['notifications']==1){echo 'selected=selected';}?>>Yes</option>
                        <option value="0" <?php if ($_SESSION['notifications']==0){echo 'selected=selected';}?>>No</option>
                    </select>
                </p>
                <p>
                    <label>Title*:</label>
                    <input name="title" id="title" type="text" required="required" readonly="true" value="<?php echo $_SESSION['title'] ?>"> </input>                 
                </p>

                <p>
                    <label>Start Date:</label>
                    <input name="startDate" id="startDate" type="date" required="required" readonly="true" value="<?php echo date('Y-m-d', strtotime($_SESSION['startDate'])); ?>" > </input>                    
                </p>

                <p>
                    <label>End Date</label>
                    <input name="endDate" id="endDate" type="date" required="required" readonly="true" value="<?php echo date('Y-m-d', strtotime($_SESSION['endDate'])); ?>"> </input>

                </p>

                <p>
                	<label>Available Start Time</label>
                	<input name="startTime" id="startTime" type="time" required="required" readonly="true" value="<?php echo date('H:i',strtotime($_SESSION['startTime'])); ?>"> </input>
                </p>

                <p>
                	<label>Available End Time</label>
                	<input name="endTime" id="endTime" type="time" required="required" readonly="true" value="<?php echo date('H:i', strtotime($_SESSION['endTime'])); ?>"> </input>
                </p>
				                
				<div class="clear"></div>
            </fieldset>


            <fieldset>
            	<legend>Question Table</legend>
            	<table id="questionTable" class="form" border="1">
            		<tr> <th>QuestionID</th> <th> Question Type </th> <th> Question Text </th> <th> Question Group </th> <th> Randomize </th> </tr>
            		<?php 
            			for($i=0; $i<count($_SESSION['questionText']); $i++){            				            			
            				echo "<tr> <td>".$i."</td> <td>".$_SESSION['questionType'][$i]."</td> <td>".$_SESSION['questionText'][$i]." </td> <td>".$_SESSION['questionGroup'][$i]." </td> <td>".$_SESSION['randomize'][$i]." </td> </tr>";
            			}
            			echo "<script> populateQuestionTable('questionTable') </script>";
            		 ?>
            		
            	</table>
            </fieldset>


            <!-- <fieldset>
            	<legend> Question </legend>
            		<table id="dataTable" class="form" border="1">                  		
                    		<tr>
                      			<p>
									<td>
										<label>Question Text</label>
										<input type="text" required="required" name="qText">
						 			</td>
                          			<td>
										<label for="qtype">Question Type</label>
											<select id="qType" name="qType" required="required" onchange="dropcheck(this)" onselect="dropcheck(this)">
												<option value = "0" selected="selected"> Slider </option>
												<option value = "1"> Free Response</option>
												<option value = "2"> Checkbox </option>
											</select>                            		
						 			</td>
								</p>
                    		</tr>                   		
                	</table>            	
            </fieldset>-->

            <fieldset class="row5">
                <legend>Submit Changes</legend>
                
                <p>
					<button class="submit" type="submit" name="add" value="add">Add Question</button>					
                </p>

                <p>
					<button class="submit" type="submit" name="commit" value="commit">Save Changes and Continue &raquo;</button>					
                </p>
				
				<div class="clear"></div>
            </fieldset>
		<?php else: ?>
		<fieldset class="row1">
			<legend>Sorry</legend>
			 <p>Some things went wrong please try again.</p>
		</fieldset>
		<?php endif; ?>
			<div class="clear"></div>
        </form>
    </body>
</html>





