<?php 
	
	session_start();
	if(!isset($_SESSION['logged_in'])){

	    header("Location: ./login.php");
	    echo 'Please Log in.';

	}//check if user is logged in


	if($_SERVER['REQUEST_METHOD'] == 'GET' && $_SESSION['valid']==true ){
		
		$studyID = $_SESSION["studyID"];		
		$_SESSION['valid']==false;
		$notifications = $_SESSION['notifications'];
		$mysqlStartDate = $_SESSION["startDate"];
		$mysqlStartTime = $_SESSION["startTime"];
		$mysqlEndDate = $_SESSION["endDate"];
		$mysqlEndTime = $_SESSION["endTime"];

		$title = $_SESSION["title"];
		$questionText = $_SESSION["questionText"];
		$questionType = $_SESSION["questionType"];
		$length = count($questionText);
		$qGroupArr = $_SESSION["questionGroup"];
		$randomizeArr = $_SESSION['randomize'];
		require_once('db.php');
		$title = mysqli_real_escape_string($con, $title);
		$sql = "INSERT INTO studyid (studyIDUnique, title, duration, notifications, startDate, endDate, availStartTime, availEndTime) VALUES ('$studyID', '$title', '0', '$notifications', '$mysqlStartDate', '$mysqlEndDate', '$mysqlStartTime', '$mysqlEndTime')";
		if(mysqli_query($con, $sql)){
			echo $sql;
			echo "<br> </br>";
			$uniquegroups = array_unique(array_values($qGroupArr), SORT_REGULAR);						
			for($i=0; $i<count(array_values($uniquegroups)); $i++){
				$uniquegroup = (string) array_values($uniquegroups)[$i];
				$surveypart = $studyID.$uniquegroup;
				$sql = "INSERT INTO studyid_parts (surveyPart, studyIDUnique) VALUES ('$surveypart', '$studyID')";
				mysqli_query($con, $sql);
				echo $sql;
				echo "<br> </br>";			
			}	
		}
		$indexarr = array();	

		for ($i=0; $i < $length; $i++) {    
		  $indexarr[$i] = "q".$i;	  
		}
		$tablename = "responses".$studyID;
		$beginning = "CREATE TABLE ".$tablename."(user varchar(500), surveyid int(20), submittime datetime, ";
		$table = implode(" varchar(500),", $indexarr);
		$sql = $beginning. $table." varchar(500));";
		echo $sql;
		echo "\n";
		if (mysqli_query($con, $sql)){		
			print "Added study id and parts and response table";
			echo "<br> </br>";
			echo "<table border=\"1\"> <tr><th>QuestionID</th> <th>QuestionType</th> <th>Question</th> <th> Question Group </th> <th> Randomize </th> </tr>";
			for($i=0; $i < $length; $i++){
				$text = mysqli_real_escape_string($con, $questionText[$i]);
				$SurveyID = $studyID . $qGroupArr[$i];
				$surveysql = "INSERT INTO surveys (SurveyID, QuestionID, QuestionType, Question, Randomize) VALUES ('$SurveyID', '$i', '$questionType[$i]', '$text', '$randomizeArr[$i]')";
				echo $surveysql;
				echo "<br>";
				if (mysqli_query($con, $surveysql)){
					print "Added question $i <br> </br>";
					echo "<tr> <td>".$i."</td> <td>".$questionType[$i]."</td> <td>".$questionText[$i]."</td> <td>".$qGroupArr[$i]."</td> <td>".$randomizeArr[$i]."</td> </tr>";
				}
				else{
					print "<tr> There was a problem adding question $i </tr>";	
				}
			}
			echo "</table>";
			$_SESSION=array();
			$_SESSION['logged_in'] = "dailydiaryliu@gmail.com";										
		}
		else{
				print "There was a problem adding this survey/studyid";	
		}								

	} 								
	
	
?>			
