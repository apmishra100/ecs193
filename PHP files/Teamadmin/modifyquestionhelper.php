 <?php
    session_start();
    if(!isset($_SESSION['logged_in']))
    {

        header("Location: ./login.php");
        echo 'Please Log in.';

    }//check if user is logged in
?>
<html>
    <head>
        <title>DiaryStudies Control Panel</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <script type="text/javascript" src="js/scripts.js"></script>

    </head>
    <body>    
        <form class="register" action="nextmod.php" id="questionForm" method="POST">
            <h1>DiaryStudies Control Panel- Modify Survey</h1>
			<?php if((isset($_POST)==true && empty($_POST)==false) || ($_SESSION['valid']==true)): 				
				if (isset($_POST['modQuest'])){
					$index = $_POST['modQuest'];
					$originalText = $_SESSION['questionText'][$index];
					$pieces ="";
					if ($_SESSION['questionType'][$index]==2){						
						$pieces = explode(";", $originalText);
						$originalText = $pieces[0];
					}					
				}
				if (isset($_POST['add'])){
					$_SESSION['valid'] = true;
					$_SESSION['modify'] = true;
					header('Location: ./modifyaddquestions.php');
					exit();
				}
				if (isset($_POST['commit'])){
					$_SESSION['valid'] = true;
					header('Location: ./modifycommit.php');
					exit();	
				}								
			?>			
            <fieldset>
            	<legend> Question </legend>
            		<table id="dataTable" class="form" border="1">                  		
                    		<tr>
                      			<p>
									<td>
										<label>Question Text</label>
										<input type="text" required="required" name="qText" value="<?php echo $originalText ?>">
						 			</td>
                          			<td>
										<label for="qtype">Question Type</label>
											<select id="qType" name="qType" required="required" onchange="dropcheck(this)" onselect="dropcheck(this)">
												<option id="opt0" value = "0"> Slider </option>
												<option id="opt1" value = "1"> Free Response</option>
												<option id="opt2" value = "2"> Checkbox </option>
												<?php echo "<script> setSelected(".$_SESSION['questionType'][$index].")</script>"; ?>
											</select>
											<?php 
													if ($_SESSION['questionType'][$index]==2){
														for($i=1; $i<count($pieces); $i++){
															if($i==1){
																$escapedString = json_encode($pieces[$i]);	
																// print_r($escapedString);
																echo "<script> setOptions($escapedString, 1)</script>";
															}
															else{
																$escapedString = json_encode($pieces[$i]);
																echo "<script> setOptions($escapedString, 0)</script>";	
															}
														}
													}
												?>                            		
						 			</td>
						 			<td>
						 				<label for="qGroup">Question Group</label>
						 				<select id="qGroup" name="qGroup" required="required">
						 					<option value = "A" <?php if ($_SESSION['questionGroup'][$index]==A){echo "selected=\"selected\"";} ?>>A</option>
						 					<option value = "B" <?php if ($_SESSION['questionGroup'][$index]==B){echo "selected=\"selected\"";} ?>>B</option>
						 					<option value = "C" <?php if ($_SESSION['questionGroup'][$index]==C){echo "selected=\"selected\"";} ?>>C</option>
						 					<option value = "D" <?php if ($_SESSION['questionGroup'][$index]==D){echo "selected=\"selected\"";} ?>>D</option>						 					
						 				</select>
						 			</td>
						 			<td>
						 				<label>Randomize</label>
						 				<input id=randomize name="randomize" type="checkbox" value="1" <?php if($_SESSION['randomize'][$index]==1){echo "checked=\"checked\"";}?>>						 				
						 			</td>

								</p>
                    		</tr>                   		
                	</table>            	
            </fieldset>

            <fieldset class="row5">
                <legend>Submit</legend>
                <p>
					<button class="submit" type="submit" name="delete" onclick="return confirm('Are you sure you wish to delete this question?')" value="<?php echo $index; ?>">Delete Question</button>					
                </p>

				<p>
					<button class="submit" type="submit" name="addmod" value="<?php echo $index; ?>">Save Changes</button>					
                </p>

				<div class="clear"></div>
            </fieldset> 
		<?php else: ?>
		<fieldset class="row1">
			<legend>Sorry</legend>
			 <p>Some things went wrong please try again.</p>
		</fieldset>
		<?php endif; ?>
			<div class="clear"></div>
        </form>
    </body>
</html>





