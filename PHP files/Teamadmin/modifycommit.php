<?php 
	
    session_start();
    if(!isset($_SESSION['logged_in']))
    {
        header("Location: ./login.php");
        echo 'Please Log in.';

    }//check if user is logged in


	if($_SERVER['REQUEST_METHOD'] == 'GET' && $_SESSION['valid']=true){
		
		$studyID = $_SESSION["studyID"];
		$notifications = $_SESSION["notifications"];
		$mysqlStartDate = $_SESSION["startDate"];
		$mysqlStartTime = $_SESSION["startTime"];
		$mysqlEndDate = $_SESSION["endDate"];
		$mysqlEndTime = $_SESSION["endTime"];

		$title = $_SESSION["title"];
		$questionText = $_SESSION["questionText"];
		$questionType = $_SESSION["questionType"];
		$qGroupArr = $_SESSION["questionGroup"];
		$randomizeArr = $_SESSION['randomize'];		
		$length = count($questionText);									
		require_once('db.php');
		if ($length ==0){
			echo "No action can be taken. If you are attemping to delete a survey, please change the available end date to a date before today.";
			exit();
		}

		$title = mysqli_real_escape_string($con, $title);
		$sql = "UPDATE studyid SET studyIDUnique='$studyID', notifications='$notifications', duration=0, title='$title', startDate='$mysqlStartDate', endDate='$mysqlEndDate', availStartTime='$mysqlStartTime', availEndTime='$mysqlEndTime'  WHERE studyIDUnique='$studyID'";
		if (mysqli_query($con, $sql)){
			print "Updated logistics";
			echo "<br>";
			$generalSurveyID = $_SESSION['studyID'].'_';
			$sql = "DELETE FROM `studyid_parts` WHERE studyIDUnique='studyID'";
			mysqli_query($con, $sql);
			$sql = "DELETE FROM surveys WHERE SurveyID LIKE '$generalSurveyID'";			

			if ($_SESSION['modified']==true){
				mysqli_query($con,$sql);
				$oldresponsetable = "responses".$studyID;
				$moddate=date('Ymd');
				$oldresponsetablenewname = $oldresponsetable."_".date("mdYHis");
				$sql = "ALTER TABLE ".$oldresponsetable." RENAME ".$oldresponsetablenewname;
				mysqli_query($con,$sql);

				$indexarr = array();	

				for ($i=0; $i < $length; $i++) {    
				  $indexarr[$i] = "q".$i;	  
				}
				$tablename = "responses".$studyID;
				$beginning = "CREATE TABLE ".$tablename."(user varchar(500), surveyid int(20), submittime datetime, ";
				$table = implode(" varchar(500),", $indexarr);
				$sql = $beginning. $table." varchar(500));";				
			}
			
			if (mysqli_query($con,$sql)){
				$uniquegroups = array_unique(array_values($qGroupArr), SORT_REGULAR);						
				for($i=0; $i<count(array_values($uniquegroups)); $i++){
					$uniquegroup = (string) array_values($uniquegroups)[$i];
					$surveypart = $studyID.$uniquegroup;
					$sql = "INSERT INTO studyid_parts (surveyPart, studyIDUnique) VALUES ('$surveypart', '$studyID')";
					mysqli_query($con, $sql);
				}
				echo "<table border=\"1\"> <tr><th>QuestionID</th> <th>QuestionType</th> <th>Question</th> <th> Question Group </th> <th> Randomize </th> </tr>";
				for($i=0; $i < $length; $i++){
					$text = mysqli_real_escape_string($con, $questionText[$i]);
					$SurveyID = $studyID . $qGroupArr[$i];
					$surveysql = "INSERT INTO surveys (SurveyID, QuestionID, QuestionType, Question, Randomize) VALUES ('$SurveyID', '$i', '$questionType[$i]', '$text', '$randomizeArr[$i]')";
					if (mysqli_query($con, $surveysql)){
						print "Added question $i <br> </br>";
						echo "<tr> <td>".$i."</td> <td>".$questionType[$i]."</td> <td>".$questionText[$i]."</td> <td>".$qGroupArr[$i]."</td> <td>".$randomizeArr[$i]."</td> </tr>";
					}
					else{
						echo "<tr> There was a problem adding question $i </tr>";	
					}
				}
				echo "</table>";					
			}
			else{
				print "Could not remove old questions\n";
				echo "<br>";
				print mysqli_error($con);
			}			
		}
		else{
				print "There was a problem adding this survey/studyid";
				echo "<br>";	
				print mysqli_error($con);
		}								
		$_SESSION=array();
		session_destroy();

	} 								
	
	
?>			
