<?php
    session_start();
    if(!isset($_SESSION['logged_in'])){

        header("Location: ./login.php");
        echo 'Please Log in.';

    }//check if user is logged in


?>

<html>
    <head>
        <title>DiaryStudies Control Panel</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <script type="text/javascript" src="js/scripts.js"></script>
    </head>
    <body>    
        <form action="next.php" class="register" method="POST">
            <h1>DiaryStudies Control Panel- Add Survey</h1>
			<?php         
            if(isset($_GET)==true && isset($_SESSION["studyID"])==true): 
				print_r($_SESSION);
				$studyID = $_SESSION["studyID"];								

			?>
            <fieldset class="row1">
                <legend>Survey Information</legend>
                <p>
                    <label>Study ID</label>
                    <input name="id" type="text" readonly="readonly" value="<?php echo $studyID ?>"/>
                </p>
                <p>
                    <label>Notifications</label>
                    <input name= "notif" type="text" readonly="readonly" value="<?php if($notifications='1'){echo 'Yes';}else{ echo 'No';}  ?>"/>                                           
                </p>

                <p>
                    <label>Start Date</label>
                    <input name= "startDate" type="text" readonly="readonly" value="<?php echo $_SESSION['startDate'] ?>"/>     
                </p>

                <p>
                    <label>End Date</label>
                    <input name= "endDate" type="text" readonly="readonly" value="<?php echo $_SESSION['endDate'] ?>"/>     
                </p>

                <p>
                    <label>Available Start Date</label>
                    <input name= "startDate" type="text" readonly="readonly" value="<?php echo $_SESSION['startTime'] ?>"/>     
                </p>

                <p>
                    <label>Available End Date</label>
                    <input name= "endDate" type="text" readonly="readonly" value="<?php echo $_SESSION['endTime'] ?>"/>     
                </p>


                <div class="clear"></div>
            </fieldset>

            <fieldset>
            	<legend> Question </legend>
            		<table id="dataTable" class="form" border="1">                  		
                    		<tr>
                      			<p>
									<td>
										<label>Question Text</label>
										<input type="text" required="required" name="qText">
						 			</td>
                          			<td>
										<label for="qtype">Question Type</label>
											<select id="qType" name="qType" required="required" onchange="dropcheck(this)" onselect="dropcheck(this)">
												<option value = "0" selected="selected"> Slider </option>
												<option value = "1"> Free Response</option>
												<option value = "2"> Checkbox </option>
											</select>
                            			<div style='display:none;' id='options'>Options <br/>
                                			<br/>
                                			<table id="opttable">
                                    			<td> <input type="text" required="required" name="opt[]"></input> </td>
                                			</table>
                                			<input type='button'  value="Add Option" onClick="addRow('opttable')" />
                                			<input type="button" value="Remove Option" onclick= "deleteRow('opttable')"> </input>
                                			<br/>
                            			</div>
						 			</td>
								</p>
                    		</tr>                   		
                	</table>            	
            </fieldset>

            <fieldset class="row5">
                <legend>Submit</legend>
                <p>
					<input class="submit" type="submit" value="Add Question &raquo;" />					
                </p>
				<div class="clear"></div>
            </fieldset> 
		<?php else: ?>
		<fieldset class="row1">
			<legend>Sorry</legend>
			 <p>Some things went wrong please try again.</p>
		</fieldset>
		<?php endif; ?>
			<div class="clear"></div>
        </form>
    </body>
</html>





