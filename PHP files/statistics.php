<?php
require_once('db.php');

header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=data.csv');

$output = fopen('php://output', 'w');

fputcsv($output, array('User ID', 'First Name', 'Last Name', 'Surveys Answered', 'Completion Percentage'));

 $statisticslist = array();
 
 $sql = 'SELECT * FROM userinfo';
 $result = mysqli_query($con, $sql);
 
 while($row = mysqli_fetch_assoc($result))
 {
   $sql = "SELECT user, surveyid, submittime FROM responses WHERE user='".$row['email']."'";
   $responses = mysqli_query($con, $sql);
   fputcsv($output, array($row['participantID'], $row['firstName'], $row['lastName'], mysqli_num_rows($responses), mysqli_num_rows($responses) / 10));
 }
?>