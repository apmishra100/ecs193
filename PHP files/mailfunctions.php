<?php
require_once "Mail.php";

function sendMail($recipientname, $recipientaddr, $subject, $body){
$from = "UC Davis Survey Service <dailydiaryliu@gmail.com>";
$to = $recipientname . " <" . $recipientaddr . ">";

$body .= PHP_EOL . PHP_EOL . 'This is an automated e-mail. You will not receive a response if you reply to this address.';

$headers = array(
    'From' => $from,
    'To' => $to,
    'Subject' => $subject
);

$smtp = Mail::factory('smtp', array(
        'host' => 'ssl://smtp.gmail.com',
        'port' => '465',
        'auth' => true,
        'username' => 'dailydiaryliu@gmail.com',
        'password' => 'davisaggies'
    ));

if (PEAR::isError($smtp)) {
  echo('<p>' . $smtp->getMessage() . '</p>');
  return false;
}
    
$mail = $smtp->send($to, $headers, $body);

if (PEAR::isError($mail)) {
    echo('<p>' . $mail->getMessage() . '</p>');
    return false;
} else {
    return true;
}
}

?>