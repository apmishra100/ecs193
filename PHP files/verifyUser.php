<?php

/*
 * Following code will create a new product row
 * All product details are read from HTTP Post Request
 */
 
// array for JSON response
$response = array();
 
if (isset($_POST['vCode']) && isset($_POST['email'])){
	$verificationCode = $_POST['vCode'];
    $email = $_POST['email'];

 
    // include db connect class
    require_once('db.php');
	
	
	
	// get a product from products table
	$emailExists = mysqli_query($con, "SELECT * FROM `userinfo` WHERE email = '$email'");
	if(mysqli_num_rows($emailExists) > 0){
		$result = mysqli_fetch_array($emailExists);
		$vCode = $result["verificationCode"];
		
		if($vCode == $verificationCode){
			$result = mysqli_query($con, "UPDATE `userinfo` SET verified=1 WHERE email='$email'");
			
			if($result){
				$response["success"] = 1;
				$response["message"] = "verified";
			}else{
				$response["success"] = 0;
				$response["message"] = "Trouble verifying user at this time.";
			}
		}else{
			$response["success"] = 0;
			$response["message"] = "verification";
		}
	}else{
		$response["success"] = 0;
		$response["message"] = "email";
	}
	
	echo json_encode($response);
}else{
	$response["success"] = 0;
	$response["message"] = "Required fields missing";
	
	echo json_encode($response);
}	
?>