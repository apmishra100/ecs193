package com.example.amanmishra.myapplication;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.support.v7.app.NotificationCompat;
import android.support.v7.app.NotificationCompat.Builder;
import android.util.Log;
import android.widget.Toast;

import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by apmis on 5/30/2016.
 */
public class Alarm extends BroadcastReceiver {
    String availstring = "";
    String email = "";
    RegisterLoginUserClass ids = new RegisterLoginUserClass();

    @Override
    public void onReceive(Context context, Intent intent) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        wl.acquire();

        if (SaveSharedPreference.getUserName(context).length() == 0) {

        }
        else {
            Intent alarmService = new Intent(context, AlarmService.class);
            context.startService(alarmService);
        }
        wl.release();
    }

    public void SetAlarm(Context context) {
        Intent dl = new Intent(context, Alarm.class);
        dl.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, dl, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * 30, pi); // Millisec * Second * Minute
    }

    public void CancelAlarm(Context context) {
        Intent dl = new Intent("com.example.amanmishra.myapplication.START_ALARM");
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, dl, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

}