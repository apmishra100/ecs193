package com.example.amanmishra.myapplication;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Sruthi on 5/25/2016.
 */
public class SaveSharedPreference
{
    static final String PREF_USER_NAME= "username";
    static final String PREF_FNAME = "firstName";
    static final String PREF_LNAME = "lastName";
    static final String SURVEYS = "surveys";

    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static void setUserName(Context ctx, String userName)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_NAME, userName);
        editor.commit();
    }

    public static String getUserName(Context ctx)
    {
        return getSharedPreferences(ctx).getString(PREF_USER_NAME, "");
    }

    public static String getSurveys(Context ctx)
    {
        return getSharedPreferences(ctx).getString(SURVEYS, "");
    }

    public static void setSurveys(Context ctx, String availSurveys)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(SURVEYS, availSurveys);
        editor.commit();
    }

    public static void clearUserName(Context ctx)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.remove(PREF_USER_NAME); //clear all stored data
        editor.commit();
    }

    public static void setNames(Context ctx, String firstName, String lastName)
       {
            SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
            editor.putString(PREF_FNAME, firstName);
            editor.commit();
            editor.putString(PREF_LNAME, lastName);
            editor.commit();
        }


    public static String getFirstName(Context ctx)
       {
            return getSharedPreferences(ctx).getString(PREF_FNAME, "");
        }

    public static String getLastName(Context ctx)
        {
            return getSharedPreferences(ctx).getString(PREF_LNAME, "");
        }

}