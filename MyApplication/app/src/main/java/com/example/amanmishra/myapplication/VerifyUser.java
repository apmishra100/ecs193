package com.example.amanmishra.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.creativityapps.gmailbackgroundlibrary.BackgroundMail;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class VerifyUser extends AppCompatActivity {
    private VerifyNewUser mAuthTask = null;
    private ResendCode rCodeTask = null;
    private boolean resend = false;

    private String email;
    private String firstName;
    private ProgressDialog pDialog;
    private EditText mCodeView;
    private URL url;
    private HttpURLConnection conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_user);

        mCodeView = (EditText) findViewById(R.id.verificationCode);
        email = getIntent().getStringExtra("email");
        firstName = getIntent().getStringExtra("firstName");

        Button verifyButton = (Button) findViewById(R.id.verify_User);
        verifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verifyUser();
            }
        });

        Button resendButton = (Button) findViewById(R.id.resend_Code);
        resendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendVerificationCode();
            }
        });
    }

    private void verifyUser(){
        if (mAuthTask != null || rCodeTask != null) {
            return;
        }

        //Reset errors
        boolean cancel = false;
        mCodeView.setError(null);

        // Store values at the time of the verification attempt.
        String vCode = mCodeView.getText().toString();

        // Check for a valid firstName.
        if (TextUtils.isEmpty(vCode)) {
            mCodeView.setError(getString(R.string.error_field_required));
            mCodeView.requestFocus();
            cancel = true;
        }

        if(cancel == false){
            // url to create new product
            try {
                url = new URL("http://ec2-54-67-34-214.us-west-1.compute.amazonaws.com/verifyUser.php");
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            mAuthTask = new VerifyNewUser(email, vCode);
            mAuthTask.execute();
        }
    }

    private void sendVerificationCode() {
        if(rCodeTask != null || mAuthTask != null)
            return;

        // url to create new product
        try {
            url = new URL("http://ec2-52-36-105-81.us-west-2.compute.amazonaws.com/resend_code.php");
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
        } catch (IOException e) {
            e.printStackTrace();
        }

        rCodeTask = new ResendCode(email);
        rCodeTask.execute();
    }

    class ResendCode extends AsyncTask<String, String, String> {
        private final String mEmail;

        private String response = " ";

        ResendCode(String mEmail) {
            this.mEmail = mEmail;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(VerifyUser.this);
            pDialog.setMessage("Sending Verification Email...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {
            try {
                StringBuilder result = new StringBuilder();
                result.append(URLEncoder.encode("email", "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(mEmail, "UTF-8"));

                // For POST only - BEGIN
                OutputStream os = conn.getOutputStream();
                BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                wr.write(result.toString());
                wr.flush();
                wr.close();
                os.close();
                // For POST only - END

                int responseCode = conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) { //success
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    response = br.readLine();
                    Log.d("VerifyUser", response);
                    br.close();

                    return "success";
                } else {
                    return "failure";
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return "failure2";
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            rCodeTask = null;
            pDialog.dismiss();
            if (file_url == "success") {
                try {
                    JSONObject obj = (JSONObject) new JSONTokener(response).nextValue();
                    int success = obj.getInt("success");
                    String msg = obj.getString("message");

                    if (success == 1) { //User successfully created
                        Toast.makeText(VerifyUser.this, "Check email for verification code.", Toast.LENGTH_LONG).show();
                    } else { //failure to insert row?
                        Toast.makeText(VerifyUser.this, "Please try again later. System could not process email credentials.", Toast.LENGTH_LONG).show();
                        finish();
                    }
                } catch (JSONException e) {
                    Log.d("Json exception", response);
                }
            } else {
                Log.d("VerifyUser", file_url);
                Toast.makeText(VerifyUser.this, "Please try again later or check wifi connectivity issues.", Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected void onCancelled() {
            rCodeTask = null;
            pDialog.dismiss();
        }

    }

    class VerifyNewUser extends AsyncTask<String, String, String> {
        private final String mEmail;
        private final String vCode;

        private String response = " ";

        VerifyNewUser(String mEmail, String vCode){
            this.mEmail = mEmail;
            this.vCode = vCode;
        }

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(VerifyUser.this);
            pDialog.setMessage("Verifying User..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Creating product
         */
        protected String doInBackground(String... args) {
            // getting JSON Object
            // Note that create product url accepts POST method
            try{
                HashMap<String, String> regData = new HashMap<>();
                regData.put("email", mEmail);
                regData.put("vCode", vCode);


                // For POST only - BEGIN
                OutputStream os = conn.getOutputStream();
                BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                wr.write(getPostDataString(regData));
                wr.flush();
                wr.close();
                os.close();
                // For POST only - END

                int responseCode = conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) { //success
                    BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    response = br.readLine();
                    Log.d("VerifyUser", response);
                    br.close();

                    return "success";
                } else {
                    return "failure";
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return "failure2";
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            mAuthTask = null;
            pDialog.dismiss();
            if(file_url == "success") {
                try {
                    JSONObject obj = (JSONObject) new JSONTokener(response).nextValue();
                    int success = obj.getInt("success");
                    String msg = obj.getString("message");

                    if(success == 1) { //User successfully created
                        Toast.makeText(VerifyUser.this, "User is now verified!", Toast.LENGTH_LONG).show();
                        Intent homescreen = new Intent(getApplicationContext(), HomeScreen.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("email", mEmail);
                        bundle.putString("firstName", firstName);
                        homescreen.putExtras(bundle);
                        startActivity(homescreen);
                    }else if(msg.contains("verification")) { //success = 0
                        mCodeView.setError(getString(R.string.error_invalid_code));
                        mCodeView.requestFocus();
                    }else{ //failure to insert row?
                        Toast.makeText(VerifyUser.this, msg, Toast.LENGTH_LONG).show();
                        finish();
                    }
                }catch(JSONException e){
                    Log.d("Json exception", response);
                }
            } else {
                Log.d("VerifyUser", file_url);
                Toast.makeText(VerifyUser.this, file_url, Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            pDialog.dismiss();
        }

        private String getPostDataString(HashMap<String, String> postDataParams) throws UnsupportedEncodingException {
            StringBuilder result = new StringBuilder();
            boolean first = true;
            for(Map.Entry<String, String> entry : postDataParams.entrySet()){
                if (first){
                    first = false;
                }
                else {
                    result.append("&");
                }
                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            }
            return result.toString();
        }
    }


}
