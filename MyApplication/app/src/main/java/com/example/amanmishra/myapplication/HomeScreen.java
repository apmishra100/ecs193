package com.example.amanmishra.myapplication;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.graphics.Color;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.w3c.dom.Text;

import java.util.HashMap;


public class HomeScreen extends AppCompatActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    Boolean clicked = false;
    String email;
    String m_surveyid;
    int grey = Color.parseColor("#303F9F");
    int blue = Color.parseColor("#606fc7");
    int white = Color.parseColor("#ffffff");
    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

    SaveSharedPreference user = new SaveSharedPreference();

    SurveyQuestionClass rluc = new SurveyQuestionClass();
    RegisterLoginUserClass ids =new RegisterLoginUserClass();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        params.setMargins(0, 30, 0, 10); //left, top, right, bottom

        getSurveys();

        Bundle b = getIntent().getExtras();
        email = b.getString("email");

        TextView fullname = (TextView) findViewById(R.id.Name);
        String s1 = user.getFirstName(this) + " ";
        String s2 = user.getLastName(this);
        fullname.setText(s1 + s2);


/*
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.home_screen);
        Button btn = new Button(getApplication().getBaseContext());
        btn.setText("Survey Info Page");
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent info = new Intent(getApplicationContext(),SurveyInfoPage.class);
                info.putExtra("email", email);
                startActivity(info);
            }
        });
        rl.addView(btn);

*/



        Button StartSurveyButton = (Button) findViewById(R.id.strtsrvbtn);
        StartSurveyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = getIntent().getExtras();
                String email = b.getString("email");
                Intent startSurvey = new Intent(getApplicationContext(), SurveyInfoPage.class);
                Bundle bundle = new Bundle();
                bundle.putString("email", email);
                startSurvey.putExtras(bundle);
                startActivity(startSurvey);
            }
        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }//oncreate


    @Override
    protected void onRestart(){
        super.onRestart();
        this.recreate();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Bundle b = getIntent().getExtras();
                String email = b.getString("email");
                /*String firstName = b.getString("firstName");*/
                Intent userSettings = new Intent(getApplicationContext(), UserSettings.class);
                Bundle bundle = new Bundle();
                bundle.putString("email", email);
                userSettings.putExtras(bundle);
                startActivity(userSettings);
                // User chose the "Settings" item, show the app settings UI...
                //return true;
/*
            case R.id.strtsrvbtn:
                Bundle b1 = getIntent().getExtras();
                String email1 = b1.getString("email");

                Intent startSurvey = new Intent(getApplicationContext(), SurveyActivity.class);
                Bundle bundle1 = new Bundle();
                bundle1.putString("email", email1);
                startSurvey.putExtras(bundle1);
                startActivity(startSurvey);
*/
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        Bundle b = getIntent().getExtras();
        String email = b.getString("email");
        savedInstanceState.putString("email", email);
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "HomeScreen Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.amanmishra.myapplication/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "HomeScreen Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.amanmishra.myapplication/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    private void getQuestions(String surveyID){
        class getQuestionTask extends AsyncTask<String, Void, String> {
            private static final String question1_URL = "http://ec2-54-67-34-214.us-west-1.compute.amazonaws.com/questionmod.php";

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... params){
                HashMap<String,String> data = new HashMap<>();
                data.put("survey", params[0]);
                String result = rluc.sendPostRequest(question1_URL, data);
                return result;
            }

            @Override
            protected void onPostExecute(String s){
                super.onPostExecute(s);
                parseString(s, rluc);
            }
        }
        getQuestionTask getQuestions = new getQuestionTask();
        getQuestions.execute(surveyID);
    }

    private void parseString(String inputQuestion, SurveyQuestionClass rluc){
//        Toast.makeText(getApplication().getBaseContext(), inputQuestion, Toast.LENGTH_LONG).show();
//        for(int i=0; i<rluc.questions.size(); i++){
//            Toast.makeText(getApplication().getBaseContext(), rluc.questions.get(i).QuestionText, Toast.LENGTH_LONG).show();
//        }

        SurveyContainer sc = rluc.questions.get(0);
        int questionType = sc.QuestionType;
        switch (questionType){
            case 0:
                int index = 0;
                Intent type0question = new Intent(getApplicationContext(),Question0Activity.class);
                type0question.putExtra("index", index);
                type0question.putExtra("questionarr", rluc.questions);
                type0question.putExtra("email", email);
                type0question.putExtra("surveyID", m_surveyid);
                startActivity(type0question);
                break;
            case 1:
                index = 0;
                Intent type1question = new Intent(getApplicationContext(),Question1Activity.class);
                type1question.putExtra("index", index);
                type1question.putExtra("questionarr", rluc.questions);
                type1question.putExtra("email", email);
                type1question.putExtra("surveyID", m_surveyid);
                startActivity(type1question);
                break;
            case 2:
                index = 0;
                Intent type2question = new Intent(getApplicationContext(),Question2Activity.class);
                type2question.putExtra("index", index);
                type2question.putExtra("questionarr", rluc.questions);
                type2question.putExtra("email", email);
                type2question.putExtra("surveyID", m_surveyid);
                startActivity(type2question);
                break;
            default:
                break;
        }
        return;
        /*String delims = "[,]";
        String[] tokens = inputQuestion.split(delims);
        for(int i=0; i<tokens.length; i++){
            tokens[i] = tokens[i].replace("\"", "");
        }

        TextView q1text = (TextView) findViewById(R.id.question1textView);
        RadioButton q1opt1 = (RadioButton) findViewById(R.id.radioButton);
        RadioButton q1opt2 = (RadioButton) findViewById(R.id.radioButton2);
        RadioButton q1opt3 = (RadioButton) findViewById(R.id.radioButton3);

        q1text.setText(tokens[0]);
        q1opt1.setText(tokens[1]);
        q1opt2.setText(tokens[2]);
        q1opt3.setText(tokens[3]);*/

    }

    private void getSurveys(){
        class getSurveyTask extends AsyncTask<String, Void, String>{
            private static final String question1_URL = "http://ec2-54-67-34-214.us-west-1.compute.amazonaws.com/getSurveys.php";

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... params){
                HashMap<String,String> data = new HashMap<>();
                data.put("email", email);
                String result = ids.sendPostRequest(question1_URL, data);
                return result;
            }

            @Override
            protected void onPostExecute(String s){
                super.onPostExecute(s);
                //Toast.makeText(getApplication().getBaseContext(), s, Toast.LENGTH_LONG).show();
                Log.d("Survey IDS", s);
                parseSurveys(s);
            }
        }
        getSurveyTask getSurveys = new getSurveyTask();
        getSurveys.execute();
    }

    private void parseSurveys(String s) {
        TextView surveytext = (TextView) findViewById(R.id.surveyTextView);
        if(s.equalsIgnoreCase("200")){
            surveytext.setText("Please check WiFi Connectivity. Surveys could not be loaded at this time.");
            return;
        }
        if(s.equalsIgnoreCase("0 results")){
            surveytext.setText("No Surveys Available. Please add surveys on the home page by clicking on the settings button.");
            return;
        }
        String delims = "_";
        String[] tokens = s.split(delims);
        surveytext.setText("Surveys open for completion:");
        LinearLayout ll = (LinearLayout) findViewById(R.id.surveylinearlayout);
        for(int i=0; i< tokens.length; i++){
            final String[] tokens2 = tokens[i].split("\\+");
            Log.d("SURVEY", tokens2[0]);
            //tokens2 = tokens[i].split("\\+");
            Log.d("SURVEY", "" + tokens2.length);
            Button btn = new Button(getApplication().getBaseContext());

            btn.setHeight(75);
            btn.setBackgroundColor(blue);
            btn.setTextColor(white);
            btn.setLayoutParams(params);
            btn.setTextSize(20);

            btn.setText(tokens2[1]+ "  id: " + tokens2[0]);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                if(!clicked){
                    clicked=true;
                    Button b = (Button) v;
                    String buttonText = tokens2[0];
                    m_surveyid = buttonText;
                    getQuestions(buttonText);
                }

                }
            });
            ll.addView(btn);
        }

        TextView popUp = new TextView(getApplication().getBaseContext());

        popUp.setGravity(Gravity.CENTER);
        popUp.setPadding(0, 20, 0, 0);
        popUp.setText(" Missing Surveys? ");
        popUp.setTextSize(15);
        popUp.setTextColor(grey);
        popUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(HomeScreen.this);
                builder.setTitle("Missing Surveys?")
                        .setMessage("If you have registered for surveys that are not appearing in your current window, the survey is most likely not open for completion."
                                + "\n\nPlease click on the My Survey tab in settings to check time availabilities on your daily surveys.")
                        .setNegativeButton("Return", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
        ll.addView(popUp);
    }


}//activity
