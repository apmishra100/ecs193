package com.example.amanmishra.myapplication;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;

/**
 * Created by apmis on 5/30/2016.
 */



public class AlarmService extends IntentService{
    Alarm alarm = new Alarm();
    RegisterLoginUserClass ids = new RegisterLoginUserClass();
    String email = "";
    public AlarmService(){
        super("Alarm Service");
    }

//    public void onCreate()
//    {
//        super.onCreate();
//    }


//    @Override
//    public void onStart(Intent intent, int startId)
//    {
//        alarm.SetAlarm(this);
//
//    }

    @Override
    protected void onHandleIntent(Intent intent) {
        email = SaveSharedPreference.getUserName(getApplicationContext());
        if (email.length()!=0){
            HashMap<String,String> data = new HashMap<>();
            data.put("email", email);
            String result = ids.sendPostRequest("http://ec2-54-67-34-214.us-west-1.compute.amazonaws.com/getSurveys.php", data);
            this.sendNotifications(getApplicationContext(), result);
        }

    }

    private void sendNotifications(Context context, String availstring){
        if(availstring.equalsIgnoreCase("200")){
            return;
        }

        if (availstring.length()==0 || availstring.equalsIgnoreCase("0 results")){
            return;
        }
        else{
            String oldavailSurveys = SaveSharedPreference.getSurveys(context);
            if (oldavailSurveys.equalsIgnoreCase(availstring)){

            }
            else if (oldavailSurveys.contains(availstring)){
                SaveSharedPreference.setSurveys(context, availstring);
            }
            else{
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
                mBuilder.setContentText("There are new surveys available");
                mBuilder.setContentTitle("New Surveys Available");
                SaveSharedPreference.setSurveys(context, availstring);
                mBuilder.setSmallIcon(R.mipmap.ic_launcher);
                mBuilder.setPriority(Notification.PRIORITY_HIGH);
                mBuilder.setVibrate(new long[0]);
                Intent notificationIntent = new Intent(context, Splash.class);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                PendingIntent pi = PendingIntent.getActivity(context,0,notificationIntent,0);
                mBuilder.setContentIntent(pi);
                NotificationManager mNotifManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                int notifid = 0;
                mNotifManager.notify(notifid, mBuilder.build());

            }
        }

    }



    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }
}