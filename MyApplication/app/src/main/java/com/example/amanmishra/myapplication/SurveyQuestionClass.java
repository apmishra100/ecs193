package com.example.amanmishra.myapplication;

import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Aman Mishra on 3/7/2016.
 */
public class SurveyQuestionClass {
    ArrayList<SurveyContainer> questions = new ArrayList<SurveyContainer>();

    public String sendPostRequest(String requestUrl, HashMap<String,String> postDataParams){
        URL url;
        String response = "";
        try{
            url = new URL(requestUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));

            writer.flush();
            writer.close();
            os.close();
            int responseCode=conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while(true){
                    String line = br.readLine();
                    if (line == null){
                        break;
                    }
                    response = response.concat(line);
                    response = response.concat("\n");
                    parseQuestion(line);
                }
            }
            else {
                response="Error Registering";
            }
            return response;
        }
        catch(Exception e){
            e.printStackTrace();
            return response;
        }
    }

    private String getPostDataString(HashMap<String, String> postDataParams) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : postDataParams.entrySet()){
            if (first){
                first = false;
            }
            else {
                result.append("&");
            }
            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        return result.toString();
    }

    public void parseQuestion(String question){
        String delims = "[\\^]";
        String[] tokens = question.split(delims);
        SurveyContainer sc = new SurveyContainer();
        sc.QuestionID = tokens[0];
        sc.QuestionType = Integer.parseInt(tokens[1]);
        sc.QuestionText = tokens[2];
        questions.add(sc);
    }
}
