package com.example.amanmishra.myapplication;

import java.io.Serializable;

/**
 * Created by Aman Mishra on 3/8/2016.
 */
public class SurveyContainer implements Serializable {
    String QuestionID;
    int QuestionType;
    String QuestionText;
    String UserAnswer;
}
