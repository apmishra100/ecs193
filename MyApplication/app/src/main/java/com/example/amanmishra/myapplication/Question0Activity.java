package com.example.amanmishra.myapplication;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class Question0Activity extends AppCompatActivity {
    int progressVal = 0;
    ArrayList<SurveyContainer> questions = new ArrayList<>();
    int index = 0;
    String email = "";
    String surveyID = "";
    Bundle bundle = new Bundle();
    boolean userInteracted = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        bundle = getIntent().getExtras();
        if (bundle != null){
            questions = (ArrayList<SurveyContainer>) bundle.getSerializable("questionarr");
            index = bundle.getInt("index");
            email = bundle.getString("email");
            surveyID=bundle.getString("surveyID");
//            if ((index - 1) >= 0 ){
//                Toast.makeText(getApplication().getBaseContext(),questions.get((index-1)).UserAnswer,Toast.LENGTH_LONG).show();
//            }
        }

        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

        super.onCreate(savedInstanceState);
        if (savedInstanceState != null){
            userInteracted = savedInstanceState.getBoolean("userInteracted");
        }
        setContentView(R.layout.activity_question0);
        //String q1answer = bundle.getString("email");
        //Toast.makeText(getApplication().getBaseContext(), q1answer, Toast.LENGTH_LONG).show();
        SeekBar mySeekBar = (SeekBar) findViewById(R.id.seekBar);
        final TextView sliderVal = (TextView) findViewById(R.id.sliderVal);
        Button mGoToNextQuestionButton = (Button) findViewById(R.id.button4);
        TextView q0text = (TextView) findViewById(R.id.question0textView);
        q0text.setText(questions.get(index).QuestionText);
        TextView q0Index = (TextView) findViewById(R.id.questionIndex);
        q0Index.setText("Question " + (index+1) + " of " + questions.size());
        mGoToNextQuestionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!userInteracted){
                    Toast.makeText(getApplication().getBaseContext(), "Please interact with the slider at least once to continue.", Toast.LENGTH_LONG).show();
                    return;
                }
                String answer = String.valueOf(progressVal);
                questions.get(index).UserAnswer=answer;
                index = index + 1;
                if (index >= questions.size()){
                    Intent submission = new Intent(getApplicationContext(), SubmissionActivity.class);
                    submission.putExtra("questionarr", questions);
                    submission.putExtra("email", email);
                    submission.putExtra("surveyID", surveyID);
                    startActivity(submission);
                }
                else{
                    SurveyContainer sc = questions.get((index));
                    int questionType = sc.QuestionType;
                    switch (questionType){
                        case 0:
                            Intent type0question = new Intent(getApplicationContext(),Question0Activity.class);
                            type0question.putExtra("index", index);
                            type0question.putExtra("questionarr", questions);
                            type0question.putExtra("email", email);
                            type0question.putExtra("surveyID", surveyID);
                            startActivity(type0question);
                            break;
                        case 1:
                            Intent type1question = new Intent(getApplicationContext(),Question1Activity.class);
                            type1question.putExtra("index", index);
                            type1question.putExtra("questionarr", questions);
                            type1question.putExtra("email", email);
                            type1question.putExtra("surveyID", surveyID);
                            startActivity(type1question);
                            break;
                        case 2:
                            Intent type2Question = new Intent (getApplicationContext(), Question2Activity.class);
                            type2Question.putExtra("index", index);
                            type2Question.putExtra("questionarr", questions);
                            type2Question.putExtra("email", email);
                            type2Question.putExtra("surveyID", surveyID);
                            startActivity(type2Question);
                        default:
                            break;
                    }
                }
            }
        });

        mySeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressVal = progress;
                sliderVal.setText("Your Answer: " + progressVal + "");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                userInteracted = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                sliderVal.setText("Your Answer: " + progressVal + "");
            }
        });
        //getQuestion2();

    }


    @Override
    protected void onRestart(){
        super.onRestart();
        this.recreate();
    }
    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState){
        savedInstanceState.putBoolean("userInteracted", userInteracted);
    }

    private void getQuestion2(){
        class getQuestionTask extends AsyncTask<String, Void, String> {
            private static final String question2_URL = "http://ec2-54-67-34-214.us-west-1.compute.amazonaws.com/question2.php";
            RegisterLoginUserClass rluc = new RegisterLoginUserClass();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... params){
                HashMap<String,String> data = new HashMap<>();
                String result = rluc.sendPostRequest(question2_URL, data);
                return result;
            }

            @Override
            protected void onPostExecute(String s){
                super.onPostExecute(s);
                parseString(s);
            }
        }
        getQuestionTask getSecondQuestion = new getQuestionTask();
        getSecondQuestion.execute();
    }

    private void parseString(String inputQuestion){
        String delims = "[,]";
        String[] tokens = inputQuestion.split(delims);
        for(int i=0; i<tokens.length; i++){
            tokens[i] = tokens[i].replace("\"", "");
        }

        TextView q0text = (TextView) findViewById(R.id.question0textView);


        q0text.setText(tokens[0]);

    }

    private void getQuestion3(){
        Bundle bundle = getIntent().getExtras();
        Intent question3intent = new Intent(getApplicationContext(), Question1Activity.class);
        String q2answer = String.valueOf(progressVal);
        bundle.putString("q2", q2answer);
        question3intent.putExtras(bundle);
        startActivity(question3intent);

    }

}


