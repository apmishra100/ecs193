package com.example.amanmishra.myapplication;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

public class SurveyInfoPage extends AppCompatActivity {

    String email;
    int blue = Color.parseColor("#606fc7");
    int white = Color.parseColor("#ffffff");
    int black = Color.parseColor("#000000");
    int red = Color.parseColor("#c31336");
    int green = Color.parseColor("#8cbf45");

    RegisterLoginUserClass ids =new RegisterLoginUserClass();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDisplayShowHomeEnabled(true);

        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

        email = getIntent().getStringExtra("email");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_info_page);
        getSurveyInfo();
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        this.recreate();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            // Handle "up" button behavior here.
            Bundle b = getIntent().getExtras();
            String email1 = b.getString("email");
                /*String firstName = b.getString("firstName");*/
            Intent homeScreen = new Intent(getApplicationContext(), HomeScreen.class);
            Bundle bundle = new Bundle();
            bundle.putString("email", email1);
            homeScreen.putExtras(bundle);
            startActivity(homeScreen);
            return true;
        } else {
            // handle other items here
        }
        return true;// return true if you handled the button click, otherwise return false.
    }


    private void getSurveyInfo(){
        class getSurveyTask extends AsyncTask<String, Void, String> {
            private static final String url = "http://ec2-54-67-34-214.us-west-1.compute.amazonaws.com/surveyInfo.php";

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... params){
                HashMap<String,String> data = new HashMap<>();
                data.put("email", email);
                String result = ids.sendPostRequest(url, data);
                return result;
            }

            @Override
            protected void onPostExecute(String s){
                super.onPostExecute(s);
                //Toast.makeText(getApplication().getBaseContext(), s, Toast.LENGTH_LONG).show();
                if(s == null){
                    Toast.makeText(SurveyInfoPage.this, "WiFi not connected. Please check connection before continuing.", Toast.LENGTH_LONG);
                    return;
                }
                Log.d("Survey IDS", s);
                parseSurveys(s);
            }
        }
        getSurveyTask getSurveys = new getSurveyTask();
        getSurveys.execute();
    }

    private void parseSurveys(String s) {
        TextView surveytext = (TextView) findViewById(R.id.surveyTextView2);
        if(s.equalsIgnoreCase("200")){
            surveytext.setText("WiFi not available. Please check connection before proceeding.");
            return;
        }
        if(s.equalsIgnoreCase("0 results")){
            surveytext.setText("No Surveys Available. Please add surveys on the home page by clicking on the settings button.");
            return;
        }
        String delims = "\\+";
        String[] tokens = s.split(delims);
        surveytext.setText("Enrolled Surveys:");
        LinearLayout ll = (LinearLayout) findViewById(R.id.surveylinearlayout2);
        for(int i=0; i< tokens.length; i++){
            TextView survey = new TextView(getApplication().getBaseContext());
            String[] tokens2 = tokens[i].split("_");
            Log.d("INFO", tokens[i]);
            // Log.d("INFO", new String(tokens2.length));
            String s1 = "\nSurvey ID: " + tokens2[0] + "\nDuration: " + tokens2[2] + " - " + tokens2[3];
            String s2 = "\nStudy Name: " + tokens2[1] + "\nTime Window: " + tokens2[4] + " - " + tokens2[5];
            //String s3 = "\nComplete for the day:" + " " + tokens2[6] + "\n";

            TextView status = new TextView(getApplication().getBaseContext());

            if( tokens2[6].equals("false") ){
                String temp = "Completed For Today";
                status.setText(temp);
                status.setTextSize(20);
                status.setTextColor(green);
            }
            else{

                String temp = "Incomplete For Today";
                status.setText(temp);
                status.setTextSize(20);
                status.setTextColor(red);
            }

            survey.setText(s1 + s2);
            survey.setTextSize(20);
            survey.setTextColor(black);

            ll.addView(survey);
            ll.addView(status);
        }
    }

}
