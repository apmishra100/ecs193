package com.example.amanmishra.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class ForgotPassword extends AppCompatActivity {
    // Progress Dialog
    private ProgressDialog pDialog;
    private ResetNewPassword mAuthTask = null;

    private EditText mEmailView;

    private URL url;
    private HttpURLConnection conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        mEmailView = (EditText) findViewById(R.id.email);

        Button forgotPasswordButton = (Button) findViewById(R.id.forgot_pass_b);
        forgotPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forgotPassword();
            }
        });
    }

    private void forgotPassword(){
        mEmailView.setError(null);

        String email = mEmailView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            // url to create new product
            try {
                url = new URL("http://ec2-52-36-105-81.us-west-2.compute.amazonaws.com/reset_password.php");
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestMethod("POST");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            mAuthTask = new ResetNewPassword(email);
            mAuthTask.execute();
        }



    }

    class ResetNewPassword extends AsyncTask<String, String, String> {
        private final String mEmail;
        private String response;

        ResetNewPassword(String mEmail) {
            this.mEmail = mEmail;
        }


        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ForgotPassword.this);
            pDialog.setMessage("Resetting Password...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Creating product
         */
        protected String doInBackground(String... args) {
            // getting JSON Object
            // Note that create product url accepts POST method
            try{
                HashMap<String, String> regData = new HashMap<>();
                regData.put("email", mEmail);

                // For POST only - BEGIN
                OutputStream os = conn.getOutputStream();
                BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                wr.write(getPostDataString(regData));
                wr.flush();
                wr.close();
                os.close();
                // For POST only - END

                int responseCode = conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) { //success
                    BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    response = br.readLine();
                    br.close();

                    return "success";
                } else {
                    return "failure";
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return "failure2";
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            mAuthTask = null;
            pDialog.dismiss();
            if(file_url == "success") {
                try {
                    JSONObject obj = (JSONObject) new JSONTokener(response).nextValue();
                    int success = obj.getInt("success");
                    String msg = obj.getString("message");

                    Log.d("LogPage", msg);

                    if(success == 1){
                        msg = "Please check your email for instructions on how to reset your password.";
                        Toast.makeText(ForgotPassword.this, msg, Toast.LENGTH_LONG).show();
                        Intent loginAgain = new Intent(getApplicationContext(), LoginPage.class);
                        startActivity(loginAgain);
                    }else{
                        mEmailView.setError(getString(R.string.error_invalid_email));
                        mEmailView.requestFocus();
                    }

                } catch(JSONException e){
                    Log.d("Json exception", response);
                }
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            pDialog.dismiss();
        }

        private String getPostDataString(HashMap<String, String> postDataParams) throws UnsupportedEncodingException {
            StringBuilder result = new StringBuilder();
            boolean first = true;
            for(Map.Entry<String, String> entry : postDataParams.entrySet()){
                if (first){
                    first = false;
                }
                else {
                    result.append("&");
                }
                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            }
            return result.toString();
        }
    }
}
