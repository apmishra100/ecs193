package com.example.amanmishra.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class DelSurveyActivity extends AppCompatActivity {
    // Progress Dialog
    private ProgressDialog pDialog;
    private DeleteReg mAuthTask = null;

    private EditText mSurveyView;
    private String email;

    private URL url;
    private HttpURLConnection conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_del_survey);

        Bundle b = getIntent().getExtras();
        email = b.getString("email");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mSurveyView = (EditText) findViewById(R.id.surveyID);

        Button delSurvey = (Button) findViewById(R.id.delSurvey);
        delSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteSurvey();
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            // Handle "up" button behavior here.
            Bundle b = getIntent().getExtras();
            String email = b.getString("email");
                /*String firstName = b.getString("firstName");*/
            Intent usersettings = new Intent(getApplicationContext(), UserSettings.class);
            Bundle bundle = new Bundle();
            bundle.putString("email", email);
            usersettings.putExtras(bundle);
            startActivity(usersettings);
            return true;
        } else {
            // handle other items here
        }
        return true;// return true if you handled the button click, otherwise return false.
    }


    private void deleteSurvey() {
        if (mAuthTask != null) {
            return;
        }

        mSurveyView.setError(null);

        String surveyID = mSurveyView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(surveyID)) {
            mSurveyView.setError(getString(R.string.error_field_required));
            focusView = mSurveyView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            // url to create new product
            try {
                url = new URL("http://ec2-54-67-34-214.us-west-1.compute.amazonaws.com/deleteSurveyID.php");
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestMethod("POST");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            mAuthTask = new DeleteReg(email,surveyID);
            mAuthTask.execute();
        }
    }

    class DeleteReg extends AsyncTask<String, String, String> {
        private final String mEmail, msurveyID;
        private String response = "";

        DeleteReg(String mEmail, String msurveyID) {
            this.mEmail = mEmail;
            this.msurveyID = msurveyID;
        }


        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(DelSurveyActivity.this);
            pDialog.setMessage("Deleting Survey ID...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Creating product
         */
        protected String doInBackground(String... args) {
            // getting JSON Object
            // Note that create product url accepts POST method
            try{
                HashMap<String, String> regData = new HashMap<>();
                regData.put("surveyID", msurveyID);
                regData.put("email", mEmail);

                // For POST only - BEGIN
                OutputStream os = conn.getOutputStream();
                BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                wr.write(getPostDataString(regData));
                wr.flush();
                wr.close();
                os.close();
                // For POST only - END

                int responseCode = conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) { //success
                    BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    String line = null;
                    while ((line = br.readLine()) != null)
                    {
                        Log.d("NewUser", line);
                        response +=line ;
                    }
                    br.close();

                    return "success";
                } else {
                    return "failure";
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return "failure2";
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            mAuthTask = null;
            pDialog.dismiss();
            if(file_url == "success") {
                try {
                    Log.d("LogPage", response);
                    JSONObject obj = (JSONObject) new JSONTokener(response).nextValue();
                    int success = obj.getInt("success");
                    String msg = obj.getString("message");

                    if(success == 1){
                        msg = "Survey deleted! Continue completing surveys on the home page! :)";
                        Toast.makeText(DelSurveyActivity.this, msg, Toast.LENGTH_LONG).show();
                        Intent homePage = new Intent(getApplicationContext(), UserSettings.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("email", mEmail);
                        homePage.putExtras(bundle);
                        startActivity(homePage);
                    }else{
                        if(msg.equalsIgnoreCase("studyID")) {
                            View focusView = null;
                            mSurveyView.setError("You are not registered for this Study ID.");
                            focusView = mSurveyView;
                            focusView.requestFocus();
                        }else{
                            String message = "Sorry database not connecting. Please try again later.";
                            Toast.makeText(DelSurveyActivity.this, message, Toast.LENGTH_LONG).show();
                        }
                    }

                } catch(JSONException e){
                    Log.d("Json exception", response);
                }
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            pDialog.dismiss();
        }

        private String getPostDataString(HashMap<String, String> postDataParams) throws UnsupportedEncodingException {
            StringBuilder result = new StringBuilder();
            boolean first = true;
            for(Map.Entry<String, String> entry : postDataParams.entrySet()){
                if (first){
                    first = false;
                }
                else {
                    result.append("&");
                }
                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            }
            return result.toString();
        }
    }
}
