package com.example.amanmishra.myapplication;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;


public class SubmissionActivity extends AppCompatActivity {

    ArrayList<SurveyContainer> questions = new ArrayList<>();
    String email = "";
    String surveyID = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submission);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            questions = (ArrayList<SurveyContainer>) bundle.getSerializable("questionarr");
            Toast.makeText(getApplication().getBaseContext(),Integer.toString(questions.size()), Toast.LENGTH_LONG).show();
            email = bundle.getString("email");
            surveyID = bundle.getString("surveyID");
            submit(questions, email, surveyID);
        }

        //Button gohome = new Button(getApplicationContext(), ge);
        Button goHome = (Button) findViewById(R.id.homescreenbtn);
        goHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = getIntent().getExtras();
                String email = b.getString("email");
                Intent gohome = new Intent(getApplicationContext(), HomeScreen.class);
                Bundle bundle = new Bundle();
                bundle.putString("email", email);
                gohome.putExtras(bundle);
                startActivity(gohome);
            }
        });

    }//onCreate


    @Override
    public void onBackPressed() {
        return;
    }

    private void submit(ArrayList<SurveyContainer> questions, String email, String surveyID){
        class SubmissionTask extends AsyncTask<Object, Void, String>{
            private static final String Submission_URL = "http://ec2-54-67-34-214.us-west-1.compute.amazonaws.com/submission.php";
            RegisterLoginUserClass rluc = new RegisterLoginUserClass();

            @Override
            protected void onPreExecute(){
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Object... params){
                HashMap<String, String> data = new HashMap<>();
                data.put("email", (String) params[1]);
                data.put("surveyID", (String) params[2]);
                ArrayList <SurveyContainer> responses = (ArrayList <SurveyContainer>) params[0];
                for(int i=0; i<responses.size(); i++){
                    String key = "q".concat(responses.get(i).QuestionID);
                    data.put(key, responses.get(i).UserAnswer);
                }
                data.put("length", Integer.toString(responses.size()));
                String result = rluc.sendPostRequest(Submission_URL, data);
                Log.d("SUBMISSION", result);
                return result;
            }

            @Override
            protected void onPostExecute(String s){
                super.onPostExecute(s);
                TextView subPost = (TextView)findViewById(R.id.submitText);
                if(s.equalsIgnoreCase("200")) {
                    subPost.setText("Check WiFi connectivity. Submission unsuccessful");
                } else {
                    subPost.setText("Thanks for completeing today's survey! Don't forget to check back again tomorrow.");
                }
            }
        }
        SubmissionTask submit = new SubmissionTask();
        submit.execute(questions, email, surveyID);
    }
}


