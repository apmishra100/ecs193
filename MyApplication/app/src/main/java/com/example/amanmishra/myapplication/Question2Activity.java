package com.example.amanmishra.myapplication;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class Question2Activity extends AppCompatActivity {
    ArrayList<SurveyContainer> questions = new ArrayList<>();
    ArrayList<String> checkedanswers = new ArrayList<>();
    int index = 0;
    String email = "";
    String surveyID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            questions = (ArrayList<SurveyContainer>) bundle.getSerializable("questionarr");
            index = bundle.getInt("index");
            email = bundle.getString("email");
            surveyID = bundle.getString("surveyID");
            if ((index - 1) >= 0 ){
                Toast.makeText(getApplication().getBaseContext(), questions.get((index - 1)).UserAnswer, Toast.LENGTH_LONG).show();
            }
        }

        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question2);
        TextView q2Index = (TextView) findViewById(R.id.questionIndex);
        q2Index.setText("Question " + (index + 1) + " of " + questions.size());
        setupView();
        Button q2button = (Button) findViewById(R.id.button6);
        q2button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkedanswers.isEmpty()){
                    Toast.makeText(getApplication().getBaseContext(), "Please select at least one option", Toast.LENGTH_SHORT).show();
                }
                else{
                    String finalanswer = "";
                    for(int i=0; i<checkedanswers.size(); i++){
                        if (i==(checkedanswers.size()-1)){
                            finalanswer = finalanswer.concat(checkedanswers.get(i));
                        }
                        else{
                            finalanswer = finalanswer.concat(checkedanswers.get(i));
                            finalanswer = finalanswer.concat(";");
                        }
                    }
                    questions.get(index).UserAnswer = finalanswer;
                    index = index + 1;
                    if (index >= questions.size()){
                        Intent submission = new Intent(getApplicationContext(), SubmissionActivity.class);
                        submission.putExtra("questionarr", questions);
                        submission.putExtra("email", email);
                        submission.putExtra("surveyID", surveyID);
                        startActivity(submission);
                    }
                    else{
                        SurveyContainer sc = questions.get(index);
                        int questionType = sc.QuestionType;
                        switch (questionType){
                            case 0:
                                Intent type0question = new Intent(getApplicationContext(),Question0Activity.class);
                                type0question.putExtra("index", index);
                                type0question.putExtra("questionarr", questions);
                                type0question.putExtra("email", email);
                                type0question.putExtra("surveyID", surveyID);
                                startActivity(type0question);
                                break;
                            case 1:
                                Intent type1question = new Intent(getApplicationContext(),Question1Activity.class);
                                type1question.putExtra("index", index);
                                type1question.putExtra("questionarr", questions);
                                type1question.putExtra("email", email);
                                type1question.putExtra("surveyID", surveyID);
                                startActivity(type1question);
                                break;
                            case 2:
                                Intent type2Question = new Intent (getApplicationContext(), Question2Activity.class);
                                type2Question.putExtra("index", index);
                                type2Question.putExtra("questionarr", questions);
                                type2Question.putExtra("email", email);
                                type2Question.putExtra("surveyID", surveyID);
                                startActivity(type2Question);
                            default:
                                break;
                        }
                    }
                }
            }
        });
    }
    @Override
    protected void onRestart(){
        super.onRestart();
        this.recreate();
    }

    public void setupView(){
        String question = questions.get(index).QuestionText;
        String delims = "[;]";
        String[] tokens = question.split(delims);
//        for(int i=0; i<tokens.length; i++){
//            Toast.makeText(getApplication().getBaseContext(), tokens[i], Toast.LENGTH_LONG).show();
//        }
        TextView q2text = (TextView) findViewById(R.id.q2textView);
        q2text.setText(tokens[0]);
        LinearLayout ll = (LinearLayout) findViewById(R.id.q2linelayout);
        for(int i=1; i<tokens.length; i++){
            CheckBox cb = new CheckBox(getApplication().getBaseContext());

            int id = Resources.getSystem().getIdentifier("btn_check_holo_light", "drawable", "android");
            cb.setButtonDrawable(id);

            cb.setText(tokens[i]);
            cb.setTextColor(Color.BLACK);
            /*cb.setBackgroundColor(Color.BLACK);*/
            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        checkedanswers.add(buttonView.getText().toString());
                    } else {
                        checkedanswers.remove(buttonView.getText().toString());
                    }
                }
            });
            ll.addView(cb);
        }
    }
}
