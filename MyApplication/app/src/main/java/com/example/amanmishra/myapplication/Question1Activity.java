package com.example.amanmishra.myapplication;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class Question1Activity extends AppCompatActivity {
    ArrayList<SurveyContainer> questions = new ArrayList<>();
    int index = 0;
    String email = "";
    String surveyID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            questions = (ArrayList<SurveyContainer>) bundle.getSerializable("questionarr");
            index = bundle.getInt("index");
            email = bundle.getString("email");
            surveyID = bundle.getString("surveyID");
//            if ((index-1) >=0){
//                Toast.makeText(getApplication().getBaseContext(),questions.get(index-1).UserAnswer,Toast.LENGTH_LONG).show();
//            }
        }

        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question1);
        TextView q1text = (TextView) findViewById(R.id.question1textView);
        q1text.setText(questions.get(index).QuestionText);
        TextView q1Index = (TextView) findViewById(R.id.questionIndex);
        q1Index.setText("Question " + (index+1) + " of " + questions.size());
        Button q1button = (Button) findViewById(R.id.button5);
        q1button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //checkAnswers();
                EditText response = (EditText) findViewById(R.id.q1response);
                if(TextUtils.isEmpty(response.getText().toString())){
                    Toast.makeText(getApplication().getBaseContext(), "Please fill a response", Toast.LENGTH_LONG).show();
                }
                else{
                    questions.get(index).UserAnswer = response.getText().toString();
                    index = index + 1;
                    if (index >= questions.size()){
                        Intent submission = new Intent(getApplicationContext(), SubmissionActivity.class);
                        submission.putExtra("questionarr", questions);
                        submission.putExtra("email", email);
                        submission.putExtra("surveyID", surveyID);
                        startActivity(submission);
                    }
                    else{
                        SurveyContainer sc = questions.get(index);
                        int questionType = sc.QuestionType;
                        switch (questionType){
                            case 0:
                                Intent type0question = new Intent(getApplicationContext(),Question0Activity.class);
                                type0question.putExtra("index", index);
                                type0question.putExtra("questionarr", questions);
                                type0question.putExtra("email", email);
                                type0question.putExtra("surveyID", surveyID);
                                startActivity(type0question);
                                break;
                            case 1:
                                Intent type1question = new Intent(getApplicationContext(),Question1Activity.class);
                                type1question.putExtra("index", index);
                                type1question.putExtra("questionarr", questions);
                                type1question.putExtra("email", email);
                                type1question.putExtra("surveyID", surveyID);
                                startActivity(type1question);
                                break;
                            case 2:
                                Intent type2Question = new Intent (getApplicationContext(), Question2Activity.class);
                                type2Question.putExtra("index", index);
                                type2Question.putExtra("questionarr", questions);
                                type2Question.putExtra("email", email);
                                type2Question.putExtra("surveyID", surveyID);
                                startActivity(type2Question);
                            default:
                                break;
                        }
                    }
                }
            }
        });
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        this.recreate();
    }

    private void getQuestion3(){
        class getQuestionTask extends AsyncTask<String, Void, String> {
            private static final String question3_URL = "http://ec2-54-67-34-214.us-west-1.compute.amazonaws.com/question3.php";
            RegisterLoginUserClass rluc = new RegisterLoginUserClass();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... params){
                HashMap<String,String> data = new HashMap<>();
                String result = rluc.sendPostRequest(question3_URL, data);
                return result;
            }

            @Override
            protected void onPostExecute(String s){
                super.onPostExecute(s);
                parseString(s);
            }
        }
        getQuestionTask getSecondQuestion = new getQuestionTask();
        getSecondQuestion.execute();
    }

    private void parseString(String inputQuestion){
        String delims = "[,]";
        String[] tokens = inputQuestion.split(delims);
        for(int i=0; i<tokens.length; i++){
            tokens[i] = tokens[i].replace("\"", "");
        }
        TextView q3text = (TextView) findViewById(R.id.question1textView);
        q3text.setText(tokens[0]);
    }

    private void checkAnswers(){
        EditText response = (EditText) findViewById(R.id.q1response);
        if(TextUtils.isEmpty(response.getText().toString())){
            Toast.makeText(getApplication().getBaseContext(), "Please fill a response", Toast.LENGTH_LONG).show();
        }
        else{
            Bundle bundle = getIntent().getExtras();
        }
    }


}

