package com.example.amanmishra.myapplication;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;




public class SurveyActivity extends AppCompatActivity {
    private ViewGroup mLinearLayout;
    String email;
    String m_surveyid;

   TextView txt_help_gest;

    SurveyQuestionClass rluc = new SurveyQuestionClass();
    RegisterLoginUserClass ids =new RegisterLoginUserClass();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

        email = getIntent().getExtras().getString("email");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);
/*
        //TextView txt_help_gest;
        txt_help_gest = (TextView) findViewById(R.id.txt_help_gest);
        // hide until its title is clicked
        txt_help_gest.setVisibility(View.GONE);
        */

        getSurveys();

        TextView sInfo = (TextView) findViewById(R.id.infoS);
        sInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(SurveyActivity.this);
                builder.setTitle("Missing Surveys?")
                        .setMessage("If you have registered for surveys that are not appearing in your current window, the survey is most likely not open for completion."
                                + "\n\nPlease click on the My Survey tab in settings to check time availabilities on your daily surveys.")
                        .setNegativeButton("Return", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        Button goHome = (Button) findViewById(R.id.homescreenbtn);
        goHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = getIntent().getExtras();
                String email = b.getString("email");
                Intent gohome = new Intent(getApplicationContext(), HomeScreen.class);
                Bundle bundle = new Bundle();
                bundle.putString("email", email);
                gohome.putExtras(bundle);
                startActivity(gohome);
            }
        });


    }//oncreate


    /**
     * onClick handler
     */
    public void toggle_contents(View v){
        if(txt_help_gest.isShown()){
            Fx.slide_up(this, txt_help_gest);
            txt_help_gest.setVisibility(View.GONE);
        }
        else{
            txt_help_gest.setVisibility(View.VISIBLE);
            Fx.slide_down(this, txt_help_gest);
        }
    }



    @Override
    protected void onRestart(){
        super.onRestart();
        this.recreate();
    }

    private void getQuestions(String surveyID){
        class getQuestionTask extends AsyncTask<String, Void, String>{
            private static final String question1_URL = "http://ec2-54-67-34-214.us-west-1.compute.amazonaws.com/questionmod.php";

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... params){
                HashMap<String,String> data = new HashMap<>();
                data.put("survey", params[0]);
                String result = rluc.sendPostRequest(question1_URL, data);
                return result;
            }

            @Override
            protected void onPostExecute(String s){
                super.onPostExecute(s);
                parseString(s, rluc);
            }
        }
        getQuestionTask getQuestions = new getQuestionTask();
        getQuestions.execute(surveyID);
    }

    private void parseString(String inputQuestion, SurveyQuestionClass rluc){
//        Toast.makeText(getApplication().getBaseContext(), inputQuestion, Toast.LENGTH_LONG).show();
//        for(int i=0; i<rluc.questions.size(); i++){
//            Toast.makeText(getApplication().getBaseContext(), rluc.questions.get(i).QuestionText, Toast.LENGTH_LONG).show();
//        }

        SurveyContainer sc = rluc.questions.get(0);
        int questionType = sc.QuestionType;
        switch (questionType){
            case 0:
                int index = 0;
                Intent type0question = new Intent(getApplicationContext(),Question0Activity.class);
                type0question.putExtra("index", index);
                type0question.putExtra("questionarr", rluc.questions);
                type0question.putExtra("email", email);
                type0question.putExtra("surveyID", m_surveyid);
                startActivity(type0question);
                break;
            case 1:
                index = 0;
                Intent type1question = new Intent(getApplicationContext(),Question1Activity.class);
                type1question.putExtra("index", index);
                type1question.putExtra("questionarr", rluc.questions);
                type1question.putExtra("email", email);
                type1question.putExtra("surveyID", m_surveyid);
                startActivity(type1question);
                break;
            case 2:
                index = 0;
                Intent type2question = new Intent(getApplicationContext(),Question2Activity.class);
                type2question.putExtra("index", index);
                type2question.putExtra("questionarr", rluc.questions);
                type2question.putExtra("email", email);
                type2question.putExtra("surveyID", m_surveyid);
                startActivity(type2question);
                break;
            default:
                break;
        }
        return;
        /*String delims = "[,]";
        String[] tokens = inputQuestion.split(delims);
        for(int i=0; i<tokens.length; i++){
            tokens[i] = tokens[i].replace("\"", "");
        }

        TextView q1text = (TextView) findViewById(R.id.question1textView);
        RadioButton q1opt1 = (RadioButton) findViewById(R.id.radioButton);
        RadioButton q1opt2 = (RadioButton) findViewById(R.id.radioButton2);
        RadioButton q1opt3 = (RadioButton) findViewById(R.id.radioButton3);

        q1text.setText(tokens[0]);
        q1opt1.setText(tokens[1]);
        q1opt2.setText(tokens[2]);
        q1opt3.setText(tokens[3]);*/

    }

    private void getSurveys(){
        class getSurveyTask extends AsyncTask<String, Void, String>{
            private static final String question1_URL = "http://ec2-54-67-34-214.us-west-1.compute.amazonaws.com/getSurveys.php";

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... params){
                HashMap<String,String> data = new HashMap<>();
                data.put("email", email);
                String result = ids.sendPostRequest(question1_URL, data);
                return result;
            }

            @Override
            protected void onPostExecute(String s){
                super.onPostExecute(s);
                //Toast.makeText(getApplication().getBaseContext(), s, Toast.LENGTH_LONG).show();
                Log.d("Survey IDS", s);
                parseSurveys(s);
            }
        }
        getSurveyTask getSurveys = new getSurveyTask();
        getSurveys.execute();
    }

    private void parseSurveys(String s) {
        TextView surveytext = (TextView) findViewById(R.id.surveyTextView);
        if(s.equalsIgnoreCase("0 results")){
            surveytext.setText("No Surveys Available. Please add surveys on the home page by clicking on the settings button.");
            return;
        }
        String delims = " ";
        String[] tokens = s.split(delims);
        surveytext.setText("Choose your Survey ID");
        LinearLayout ll = (LinearLayout) findViewById(R.id.surveylinearlayout);

        for(int i=0; i< tokens.length; i++){
            Button btn = new Button(getApplication().getBaseContext());
            btn.setText(tokens[i]);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Button b = (Button)v;
                    String buttonText = b.getText().toString();
                    m_surveyid = buttonText;
                    getQuestions(buttonText);
                }
            });
            ll.addView(btn);
        }

    }


}
